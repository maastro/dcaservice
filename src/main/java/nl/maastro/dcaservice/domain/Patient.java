package nl.maastro.dcaservice.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import nl.maastro.dcaservice.domain.enumeration.Gender;

/**
 * A Patient.
 */
@Entity
@Table(name = "patient")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Patient implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "patient_uid")
    private String patientUid;

    @Column(name = "bsn")
    private String bsn;

    @Column(name = "name_initials")
    private String nameInitials;

    @Column(name = "name_family_prefix")
    private String nameFamilyPrefix;

    @Column(name = "name_family")
    private String nameFamily;

    @Enumerated(EnumType.STRING)
    @Column(name = "gender")
    private Gender gender;

    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;

    @OneToMany(mappedBy = "patient")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<RnvPlan> rnvPlans = new HashSet<>();

    @OneToMany(mappedBy = "patient")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<PatientObservation> patientObservations = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPatientUid() {
        return patientUid;
    }

    public Patient patientUid(String patientUid) {
        this.patientUid = patientUid;
        return this;
    }

    public void setPatientUid(String patientUid) {
        this.patientUid = patientUid;
    }

    public String getBsn() {
        return bsn;
    }

    public Patient bsn(String bsn) {
        this.bsn = bsn;
        return this;
    }

    public void setBsn(String bsn) {
        this.bsn = bsn;
    }

    public String getNameInitials() {
        return nameInitials;
    }

    public Patient nameInitials(String nameInitials) {
        this.nameInitials = nameInitials;
        return this;
    }

    public void setNameInitials(String nameInitials) {
        this.nameInitials = nameInitials;
    }

    public String getNameFamilyPrefix() {
        return nameFamilyPrefix;
    }

    public Patient nameFamilyPrefix(String nameFamilyPrefix) {
        this.nameFamilyPrefix = nameFamilyPrefix;
        return this;
    }

    public void setNameFamilyPrefix(String nameFamilyPrefix) {
        this.nameFamilyPrefix = nameFamilyPrefix;
    }

    public String getNameFamily() {
        return nameFamily;
    }

    public Patient nameFamily(String nameFamily) {
        this.nameFamily = nameFamily;
        return this;
    }

    public void setNameFamily(String nameFamily) {
        this.nameFamily = nameFamily;
    }

    public Gender getGender() {
        return gender;
    }

    public Patient gender(Gender gender) {
        this.gender = gender;
        return this;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public Patient dateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
        return this;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Set<RnvPlan> getRnvPlans() {
        return rnvPlans;
    }

    public Patient rnvPlans(Set<RnvPlan> rnvPlans) {
        this.rnvPlans = rnvPlans;
        return this;
    }

    public Patient addRnvPlan(RnvPlan rnvPlan) {
        this.rnvPlans.add(rnvPlan);
        rnvPlan.setPatient(this);
        return this;
    }

    public Patient removeRnvPlan(RnvPlan rnvPlan) {
        this.rnvPlans.remove(rnvPlan);
        rnvPlan.setPatient(null);
        return this;
    }

    public void setRnvPlans(Set<RnvPlan> rnvPlans) {
        this.rnvPlans = rnvPlans;
    }

    public Set<PatientObservation> getPatientObservations() {
        return patientObservations;
    }

    public Patient patientObservations(Set<PatientObservation> patientObservations) {
        this.patientObservations = patientObservations;
        return this;
    }

    public Patient addPatientObservations(PatientObservation patientObservation) {
        this.patientObservations.add(patientObservation);
        patientObservation.setPatient(this);
        return this;
    }

    public Patient removePatientObservations(PatientObservation patientObservation) {
        this.patientObservations.remove(patientObservation);
        patientObservation.setPatient(null);
        return this;
    }

    public void setPatientObservations(Set<PatientObservation> patientObservations) {
        this.patientObservations = patientObservations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Patient patient = (Patient) o;
        if (patient.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), patient.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Patient{" +
            "id=" + getId() +
            ", patientUid='" + getPatientUid() + "'" +
            ", bsn='" + getBsn() + "'" +
            ", nameInitials='" + getNameInitials() + "'" +
            ", nameFamilyPrefix='" + getNameFamilyPrefix() + "'" +
            ", nameFamily='" + getNameFamily() + "'" +
            ", gender='" + getGender() + "'" +
            ", dateOfBirth='" + getDateOfBirth() + "'" +
            "}";
    }
}
