package nl.maastro.dcaservice.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import nl.maastro.dcaservice.domain.enumeration.DicaRegistration;

/**
 * A DicaRegistrationEntity.
 */
@Entity
@Table(name = "dica_registration_entity")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DicaRegistrationEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "dica_registration")
    private DicaRegistration dicaRegistration;

    @ManyToMany(mappedBy = "dicaRegistrations")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<RnvPlan> dicaRegistrations = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DicaRegistration getDicaRegistration() {
        return dicaRegistration;
    }

    public DicaRegistrationEntity dicaRegistration(DicaRegistration dicaRegistration) {
        this.dicaRegistration = dicaRegistration;
        return this;
    }

    public void setDicaRegistration(DicaRegistration dicaRegistration) {
        this.dicaRegistration = dicaRegistration;
    }

    public Set<RnvPlan> getDicaRegistrations() {
        return dicaRegistrations;
    }

    public DicaRegistrationEntity dicaRegistrations(Set<RnvPlan> rnvPlans) {
        this.dicaRegistrations = rnvPlans;
        return this;
    }

    public DicaRegistrationEntity addDicaRegistration(RnvPlan rnvPlan) {
        this.dicaRegistrations.add(rnvPlan);
        rnvPlan.getDicaRegistrations().add(this);
        return this;
    }

    public DicaRegistrationEntity removeDicaRegistration(RnvPlan rnvPlan) {
        this.dicaRegistrations.remove(rnvPlan);
        rnvPlan.getDicaRegistrations().remove(this);
        return this;
    }

    public void setDicaRegistrations(Set<RnvPlan> rnvPlans) {
        this.dicaRegistrations = rnvPlans;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DicaRegistrationEntity dicaRegistrationEntity = (DicaRegistrationEntity) o;
        if (dicaRegistrationEntity.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dicaRegistrationEntity.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DicaRegistrationEntity{" +
            "id=" + getId() +
            ", dicaRegistration='" + getDicaRegistration() + "'" +
            "}";
    }
}
