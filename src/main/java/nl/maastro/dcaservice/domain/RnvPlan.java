package nl.maastro.dcaservice.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A RnvPlan.
 */
@Entity
@Table(name = "rnv_plan")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class RnvPlan implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "plan_label")
    private String planLabel;

    @Column(name = "plan_sop_instance_uid")
    private String planSopInstanceUid;

    @Column(name = "study_instance_uid")
    private String studyInstanceUid;

    @Column(name = "date_created")
    private LocalDate dateCreated;

    @ManyToOne
    private Patient patient;

    @OneToMany(mappedBy = "rnvPlan")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<PlanObservation> planObservations = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "rnv_plan_dica_registrations",
               joinColumns = @JoinColumn(name="rnv_plans_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="dica_registrations_id", referencedColumnName="id"))
    private Set<DicaRegistrationEntity> dicaRegistrations = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPlanLabel() {
        return planLabel;
    }

    public RnvPlan planLabel(String planLabel) {
        this.planLabel = planLabel;
        return this;
    }

    public void setPlanLabel(String planLabel) {
        this.planLabel = planLabel;
    }

    public String getPlanSopInstanceUid() {
        return planSopInstanceUid;
    }

    public RnvPlan planSopInstanceUid(String planSopInstanceUid) {
        this.planSopInstanceUid = planSopInstanceUid;
        return this;
    }

    public void setPlanSopInstanceUid(String planSopInstanceUid) {
        this.planSopInstanceUid = planSopInstanceUid;
    }

    public String getStudyInstanceUid() {
        return studyInstanceUid;
    }

    public RnvPlan studyInstanceUid(String studyInstanceUid) {
        this.studyInstanceUid = studyInstanceUid;
        return this;
    }

    public void setStudyInstanceUid(String studyInstanceUid) {
        this.studyInstanceUid = studyInstanceUid;
    }

    public LocalDate getDateCreated() {
        return dateCreated;
    }

    public RnvPlan dateCreated(LocalDate dateCreated) {
        this.dateCreated = dateCreated;
        return this;
    }

    public void setDateCreated(LocalDate dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Patient getPatient() {
        return patient;
    }

    public RnvPlan patient(Patient patient) {
        this.patient = patient;
        return this;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Set<PlanObservation> getPlanObservations() {
        return planObservations;
    }

    public RnvPlan planObservations(Set<PlanObservation> planObservations) {
        this.planObservations = planObservations;
        return this;
    }

    public RnvPlan addPlanObservations(PlanObservation planObservation) {
        this.planObservations.add(planObservation);
        planObservation.setRnvPlan(this);
        return this;
    }

    public RnvPlan removePlanObservations(PlanObservation planObservation) {
        this.planObservations.remove(planObservation);
        planObservation.setRnvPlan(null);
        return this;
    }

    public void setPlanObservations(Set<PlanObservation> planObservations) {
        this.planObservations = planObservations;
    }

    public Set<DicaRegistrationEntity> getDicaRegistrations() {
        return dicaRegistrations;
    }

    public RnvPlan dicaRegistrations(Set<DicaRegistrationEntity> dicaRegistrationEntities) {
        this.dicaRegistrations = dicaRegistrationEntities;
        return this;
    }

    public RnvPlan addDicaRegistrations(DicaRegistrationEntity dicaRegistrationEntity) {
        this.dicaRegistrations.add(dicaRegistrationEntity);
        dicaRegistrationEntity.getDicaRegistrations().add(this);
        return this;
    }

    public RnvPlan removeDicaRegistrations(DicaRegistrationEntity dicaRegistrationEntity) {
        this.dicaRegistrations.remove(dicaRegistrationEntity);
        dicaRegistrationEntity.getDicaRegistrations().remove(this);
        return this;
    }

    public void setDicaRegistrations(Set<DicaRegistrationEntity> dicaRegistrationEntities) {
        this.dicaRegistrations = dicaRegistrationEntities;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RnvPlan rnvPlan = (RnvPlan) o;
        if (rnvPlan.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), rnvPlan.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RnvPlan{" +
            "id=" + getId() +
            ", planLabel='" + getPlanLabel() + "'" +
            ", planSopInstanceUid='" + getPlanSopInstanceUid() + "'" +
            ", studyInstanceUid='" + getStudyInstanceUid() + "'" +
            ", dateCreated='" + getDateCreated() + "'" +
            "}";
    }
}
