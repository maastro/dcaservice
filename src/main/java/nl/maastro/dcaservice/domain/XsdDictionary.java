package nl.maastro.dcaservice.domain;

import java.util.HashMap;
import java.util.Map;

public class XsdDictionary {
    
    private String projectName;
    private String projectVersion;
    private String projectUid;
    private String projectUri;
    private String filePath;
    private Map<String, XsdDataElement> dataDictionary = new HashMap<>();
    
    public String getProjectName() {
        return projectName;
    }
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
    public String getProjectVersion() {
        return projectVersion;
    }
    public void setProjectVersion(String projectVersion) {
        this.projectVersion = projectVersion;
    }
    public String getProjectUid() {
        return projectUid;
    }
    public void setProjectUid(String projectUid) {
        this.projectUid = projectUid;
    }
    public String getProjectUri() {
        return projectUri;
    }
    public void setProjectUri(String projectUri) {
        this.projectUri = projectUri;
    }
    public String getFilePath() {
        return filePath;
    }
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    public Map<String, XsdDataElement> getDataDictionary() {
        return dataDictionary;
    }
    public void setDataDictionary(Map<String, XsdDataElement> dataDictionary) {
        this.dataDictionary = dataDictionary;
    }
    
    @Override
    public String toString() {
        return "XsdDictionary [projectName=" + projectName + ", projectVersion=" + projectVersion + ", projectUid="
                + projectUid + ", projectUri=" + projectUri + ", filePath=" + filePath + ", dataDictionary="
                + dataDictionary + "]";
    }
}
