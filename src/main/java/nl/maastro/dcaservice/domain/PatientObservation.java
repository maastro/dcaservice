package nl.maastro.dcaservice.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A PatientObservation.
 */
@Entity
@Table(name = "patient_observation")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PatientObservation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "observation_identifier")
    private String observationIdentifier;

    @Column(name = "observation_value")
    private String observationValue;

    @Column(name = "observation_date")
    private Instant observationDate;

    @ManyToOne
    private Patient patient;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getObservationIdentifier() {
        return observationIdentifier;
    }

    public PatientObservation observationIdentifier(String observationIdentifier) {
        this.observationIdentifier = observationIdentifier;
        return this;
    }

    public void setObservationIdentifier(String observationIdentifier) {
        this.observationIdentifier = observationIdentifier;
    }

    public String getObservationValue() {
        return observationValue;
    }

    public PatientObservation observationValue(String observationValue) {
        this.observationValue = observationValue;
        return this;
    }

    public void setObservationValue(String observationValue) {
        this.observationValue = observationValue;
    }

    public Instant getObservationDate() {
        return observationDate;
    }

    public PatientObservation observationDate(Instant observationDate) {
        this.observationDate = observationDate;
        return this;
    }

    public void setObservationDate(Instant observationDate) {
        this.observationDate = observationDate;
    }

    public Patient getPatient() {
        return patient;
    }

    public PatientObservation patient(Patient patient) {
        this.patient = patient;
        return this;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PatientObservation patientObservation = (PatientObservation) o;
        if (patientObservation.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), patientObservation.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PatientObservation{" +
            "id=" + getId() +
            ", observationIdentifier='" + getObservationIdentifier() + "'" +
            ", observationValue='" + getObservationValue() + "'" +
            ", observationDate='" + getObservationDate() + "'" +
            "}";
    }
}
