package nl.maastro.dcaservice.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import nl.maastro.dcaservice.domain.enumeration.TransactionStatus;

/**
 * A FhirTransactionLog.
 */
@Entity
@Table(name = "fhir_transaction_log")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class FhirTransactionLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "jhi_date")
    private Instant date;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private TransactionStatus status;

    @Column(name = "plan_uid")
    private String planUid;

    @Lob
    @Column(name = "outgoing_bundle")
    private String outgoingBundle;

    @Lob
    @Column(name = "reponse_bundle")
    private String reponseBundle;

    @Column(name = "error_message")
    private String errorMessage;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDate() {
        return date;
    }

    public FhirTransactionLog date(Instant date) {
        this.date = date;
        return this;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public TransactionStatus getStatus() {
        return status;
    }

    public FhirTransactionLog status(TransactionStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(TransactionStatus status) {
        this.status = status;
    }

    public String getPlanUid() {
        return planUid;
    }

    public FhirTransactionLog planUid(String planUid) {
        this.planUid = planUid;
        return this;
    }

    public void setPlanUid(String planUid) {
        this.planUid = planUid;
    }

    public String getOutgoingBundle() {
        return outgoingBundle;
    }

    public FhirTransactionLog outgoingBundle(String outgoingBundle) {
        this.outgoingBundle = outgoingBundle;
        return this;
    }

    public void setOutgoingBundle(String outgoingBundle) {
        this.outgoingBundle = outgoingBundle;
    }

    public String getReponseBundle() {
        return reponseBundle;
    }

    public FhirTransactionLog reponseBundle(String reponseBundle) {
        this.reponseBundle = reponseBundle;
        return this;
    }

    public void setReponseBundle(String reponseBundle) {
        this.reponseBundle = reponseBundle;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public FhirTransactionLog errorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
        return this;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FhirTransactionLog fhirTransactionLog = (FhirTransactionLog) o;
        if (fhirTransactionLog.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), fhirTransactionLog.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "FhirTransactionLog{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", status='" + getStatus() + "'" +
            ", planUid='" + getPlanUid() + "'" +
            ", outgoingBundle='" + getOutgoingBundle() + "'" +
            ", reponseBundle='" + getReponseBundle() + "'" +
            ", errorMessage='" + getErrorMessage() + "'" +
            "}";
    }
}
