package nl.maastro.dcaservice.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A PlanObservation.
 */
@Entity
@Table(name = "plan_observation")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PlanObservation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "observation_identifier")
    private String observationIdentifier;

    @Column(name = "observation_value")
    private String observationValue;

    @Column(name = "observation_date")
    private Instant observationDate;

    @ManyToOne
    private RnvPlan rnvPlan;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getObservationIdentifier() {
        return observationIdentifier;
    }

    public PlanObservation observationIdentifier(String observationIdentifier) {
        this.observationIdentifier = observationIdentifier;
        return this;
    }

    public void setObservationIdentifier(String observationIdentifier) {
        this.observationIdentifier = observationIdentifier;
    }

    public String getObservationValue() {
        return observationValue;
    }

    public PlanObservation observationValue(String observationValue) {
        this.observationValue = observationValue;
        return this;
    }

    public void setObservationValue(String observationValue) {
        this.observationValue = observationValue;
    }

    public Instant getObservationDate() {
        return observationDate;
    }

    public PlanObservation observationDate(Instant observationDate) {
        this.observationDate = observationDate;
        return this;
    }

    public void setObservationDate(Instant observationDate) {
        this.observationDate = observationDate;
    }

    public RnvPlan getRnvPlan() {
        return rnvPlan;
    }

    public PlanObservation rnvPlan(RnvPlan rnvPlan) {
        this.rnvPlan = rnvPlan;
        return this;
    }

    public void setRnvPlan(RnvPlan rnvPlan) {
        this.rnvPlan = rnvPlan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PlanObservation planObservation = (PlanObservation) o;
        if (planObservation.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), planObservation.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PlanObservation{" +
            "id=" + getId() +
            ", observationIdentifier='" + getObservationIdentifier() + "'" +
            ", observationValue='" + getObservationValue() + "'" +
            ", observationDate='" + getObservationDate() + "'" +
            "}";
    }
}
