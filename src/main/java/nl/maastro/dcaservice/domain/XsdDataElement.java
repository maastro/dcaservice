package nl.maastro.dcaservice.domain;

import java.util.Map;

import nl.maastro.dcaservice.domain.enumeration.ObservationDataType;

public class XsdDataElement {

	private String identifier;
	private String description;
	private ObservationDataType dataType;
	private String valueTypeIdentifier;
	private Map<String, String> codingRestrictions;
    
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public ObservationDataType getDataType() {
		return dataType;
	}
	public void setDataType(ObservationDataType dataType) {
	    
		this.dataType = dataType;
	}
	public String getValueTypeIdentifier() {
        return valueTypeIdentifier;
    }
    public void setValueTypeIdentifier(String valueTypeIdentifier) {
        this.valueTypeIdentifier = valueTypeIdentifier;
    }
    public Map<String, String> getCodingRestrictions() {
        return codingRestrictions;
    }
    public void setCodingRestrictions(Map<String, String> codingRestrictions) {
        this.codingRestrictions = codingRestrictions;
    }
    
    @Override
    public String toString() {
        return "XsdDataElement [identifier=" + identifier + ", description=" + description + ", dataType=" + dataType
                + ", valueTypeIdentifier=" + valueTypeIdentifier + ", codingRestrictions=" + codingRestrictions + "]";
    }
}
