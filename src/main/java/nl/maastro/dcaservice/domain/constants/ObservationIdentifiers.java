package nl.maastro.dcaservice.domain.constants;

public final class ObservationIdentifiers {

	// Patient observation identifiers
	public static final String DECEASE_DATE = "datovl";

	// Plan observation identifiers
	public static final String CTSCAN_DATE = "ctscandat";
	public static final String START_THORAX = "startthorax";
	public static final String END_THORAX = "eindthorax";
	public static final String APPA = "bstrtchna";
	public static final String CRT_3D = "bstrtchnb";
	public static final String IMRT = "bstrtchnc";
	public static final String VMAT = "bstrtchnd";
	public static final String MEAN_LUNG_DOSE = "mld";
	public static final String MAX_OESOPHAGUS_DOSE = "maxoes";
	public static final String CUMULATIVE_DOSE = "cumdosis";
	public static final String IS_PLANNED_CUMULATIVE_DOSE = "geplcumdosis";

}
