package nl.maastro.dcaservice.domain.constants;

import org.hl7.fhir.dstu3.model.Coding;

/**
 * Coding constants for FHIR procedures (RT-verrichtingencodes).
 */
public final class RtProcedureCodings {
	
	private static final String NCI_THESAURUS_URI = "http://ncicb.nci.nih.gov/xml/owl/EVS/Thesaurus.owl";   
	private static final String ROO_URI = "http://www.cancerdata.org/roo";
	
	public static final Coding EXTERNAL_BEAM_RADIATION_THERAPY = new Coding(
			NCI_THESAURUS_URI,
			"C15751",
			"External Beam Radiation Therapy");
	
	public static final Coding LUNG_CARCINOMA = new Coding(
			NCI_THESAURUS_URI,
			"C4878",
			"Lung Carcinoma");
	
	public static final Coding PARALLEL_OPPOSED_FIELDS_RT = new Coding(
			ROO_URI,
			"100350",
			"Parallel-Opposed Fields Radiation Therapy");
	
	public static final Coding CRT_3D = new Coding(
			NCI_THESAURUS_URI,
			"C16035",
			"3-Dimensional Conformal Radiation Therapy");
	
	public static final Coding IMRT = new Coding(
			NCI_THESAURUS_URI,
			"C16135",
			"Intensity-Modulated Radiation Therapy");
	
	public static final Coding VMAT = new Coding(
			NCI_THESAURUS_URI,
			"C104933",
			"Volume Modulated Arc Therapy");
}
