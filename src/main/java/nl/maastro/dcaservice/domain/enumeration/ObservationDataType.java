package nl.maastro.dcaservice.domain.enumeration;

public enum ObservationDataType {
	BOOLEAN, DECIMAL, INTEGER, DATE, DATETIME, TIME, STRING, VALUECODING, VALUEQUANTITY;
}
