package nl.maastro.dcaservice.domain.enumeration;

/**
 * The Gender enumeration.
 */
public enum Gender {
    MALE,FEMALE,OTHER
}
