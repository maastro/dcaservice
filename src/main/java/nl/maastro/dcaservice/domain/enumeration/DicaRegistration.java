package nl.maastro.dcaservice.domain.enumeration;

/**
 * The DicaRegistration enumeration.
 */
public enum DicaRegistration {
    DLCA
}
