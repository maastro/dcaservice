package nl.maastro.dcaservice.domain.enumeration;

/**
 * The TransactionStatus enumeration.
 */
public enum TransactionStatus {
    SUCCESS,FAILED
}
