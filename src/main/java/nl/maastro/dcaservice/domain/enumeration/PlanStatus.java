package nl.maastro.dcaservice.domain.enumeration;

/**
 * The PlanStatus enumeration.
 */
public enum PlanStatus {
    UNKNOWN,IN_PROGRESS,ABORTED,COMPLETED,ENTERED_IN_ERROR
}
