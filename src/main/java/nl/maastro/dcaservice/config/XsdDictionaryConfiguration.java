package nl.maastro.dcaservice.config;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.annotation.PostConstruct;
import javax.xml.transform.stream.StreamSource;

import org.apache.ws.commons.schema.XmlSchema;
import org.apache.ws.commons.schema.XmlSchemaAnnotated;
import org.apache.ws.commons.schema.XmlSchemaAnnotationItem;
import org.apache.ws.commons.schema.XmlSchemaAttribute;
import org.apache.ws.commons.schema.XmlSchemaAttributeOrGroupRef;
import org.apache.ws.commons.schema.XmlSchemaCollection;
import org.apache.ws.commons.schema.XmlSchemaComplexType;
import org.apache.ws.commons.schema.XmlSchemaDocumentation;
import org.apache.ws.commons.schema.XmlSchemaElement;
import org.apache.ws.commons.schema.XmlSchemaFacet;
import org.apache.ws.commons.schema.XmlSchemaSequence;
import org.apache.ws.commons.schema.XmlSchemaSequenceMember;
import org.apache.ws.commons.schema.XmlSchemaSimpleType;
import org.apache.ws.commons.schema.XmlSchemaSimpleTypeRestriction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import nl.maastro.dcaservice.domain.XsdDataElement;
import nl.maastro.dcaservice.domain.XsdDictionary;
import nl.maastro.dcaservice.domain.enumeration.DicaRegistration;
import nl.maastro.dcaservice.domain.enumeration.ObservationDataType;
import nl.maastro.dcaservice.service.DicaRegistrationService;

@Configuration
@EnableConfigurationProperties(XsdDictionaryProperties.class)
public class XsdDictionaryConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(DicaRegistrationService.class);

    private Map<DicaRegistration, XsdDictionary> xsdDictionaries = new HashMap<>();

    private static final String COMPLEX_TYPE_EXPORT_HEADER = "exportheaderElement";
    private static final String COMPLEX_TYPE_PATIENT = "patient_type";
    private static final String COMPLEX_TYPE_BEHANDELING = "behandeling_type";

    private static final String ATTRIBUTE_PROJECTNAME = "projectname";
    private static final String ATTRIBUTE_PROJECTVERSION = "projectversion";
    private static final String ATTRIBUTE_PROJECTUID = "projectUID";
    
    @Autowired
    XsdDictionaryProperties xsdDictionaryProperties;

    @PostConstruct
    public void initialize() {
        for (Map.Entry<String, String> entry : xsdDictionaryProperties.getXsdDictionary().entrySet()) {
            String dicaRegistrationString = entry.getKey();
            String xsdDictionaryFile = entry.getValue();

            DicaRegistration dicaRegistration;
            try {
                dicaRegistration = DicaRegistration.valueOf(dicaRegistrationString.toUpperCase());
            } catch (IllegalArgumentException e) {
                logger.warn("Unknown DICA registration '" + dicaRegistrationString + "'; XSD dictionary '"
                        + xsdDictionaryFile + "' will not be read", e);
                continue;
            }
            
            XsdDictionary xsdDictionary = readXsdDictionary(xsdDictionaryFile);
            xsdDictionaries.put(dicaRegistration, xsdDictionary);
        }
    }

    private XsdDictionary readXsdDictionary(String filePath) {
        logger.info("Reading XSD dictionary file '" + filePath + "'");
        try {
            XsdDictionary xsdDictionary = new XsdDictionary(); 
            xsdDictionary.setFilePath(filePath);
            
            InputStream stream = new FileInputStream(filePath);
            XmlSchemaCollection schemaCol = new XmlSchemaCollection();
            XmlSchema schema = schemaCol.read(new StreamSource(stream));
            
            XmlSchemaComplexType exportHeader = (XmlSchemaComplexType) schema.getTypeByName(COMPLEX_TYPE_EXPORT_HEADER);
            xsdDictionary.setProjectName(readComplexTypeAttribute(exportHeader, ATTRIBUTE_PROJECTNAME));
            xsdDictionary.setProjectVersion(readComplexTypeAttribute(exportHeader, ATTRIBUTE_PROJECTVERSION));
            xsdDictionary.setProjectUid(readComplexTypeAttribute(exportHeader, ATTRIBUTE_PROJECTUID));
            xsdDictionary.setProjectUri(schema.getTargetNamespace());
            
            Map<String, XsdDataElement> patientXsdDataElements = readComplexTypeSequence(
                    (XmlSchemaComplexType) schema.getTypeByName(COMPLEX_TYPE_PATIENT));
            Map<String, XsdDataElement> planXsdDataElements = readComplexTypeSequence(
                    (XmlSchemaComplexType) schema.getTypeByName(COMPLEX_TYPE_BEHANDELING));
            xsdDictionary.getDataDictionary().putAll(patientXsdDataElements);
            xsdDictionary.getDataDictionary().putAll(planXsdDataElements);
            
            return xsdDictionary;
            
        } catch (Exception e) {
            logger.error("Unable to read XSD dictionary '" + filePath + "'", e);
            return null;
        }
    }
    
    private String readComplexTypeAttribute(XmlSchemaComplexType complexType, String attributeName) {
        for (XmlSchemaAttributeOrGroupRef attribute : complexType.getAttributes()) {
            XmlSchemaAttribute xmlSchemaAttribute = (XmlSchemaAttribute) attribute;
            if (xmlSchemaAttribute.getName().equals(attributeName)) {
                return xmlSchemaAttribute.getFixedValue();
            }
        }
        throw new NoSuchElementException("Unable to find attribute '" + attributeName 
                + "' for complexType: " + complexType.toString());
    }
    
    private Map<String, XsdDataElement> readComplexTypeSequence(XmlSchemaComplexType complexType) throws Exception {
        Map<String, XsdDataElement> xsdDataElements = new HashMap<>();
        try {
            XmlSchemaSequence complexTypeSeqeuence = (XmlSchemaSequence) complexType.getParticle();
            for (XmlSchemaSequenceMember complexTypeSequenceMember : complexTypeSeqeuence.getItems()) {
                XmlSchemaElement complexTypeElement = (XmlSchemaElement) complexTypeSequenceMember;
                XsdDataElement xsdDataElement = readComplexTypeElement(complexTypeElement);
                xsdDataElements.put(xsdDataElement.getIdentifier(), xsdDataElement);
            }
            return xsdDataElements;            
        } catch (ClassCastException e) {
            throw new Exception("Unable to parse complexType: " + complexType.toString());
        }
    }

    private XsdDataElement readComplexTypeElement(XmlSchemaElement element) throws Exception {
        XsdDataElement xsdDataElement = new XsdDataElement();
        xsdDataElement.setIdentifier(element.getName());
        xsdDataElement.setDescription(readAnnotatedElementDocumentation(element));
        if (element.getSchemaType() instanceof XmlSchemaSimpleType) {
            XmlSchemaSimpleType elementSchemaType = (XmlSchemaSimpleType) element.getSchemaType();
            String elementSchemaTypeName = elementSchemaType.getName();
            
            switch (elementSchemaTypeName.toUpperCase()) {
            case "BOOLEAN":
                xsdDataElement.setDataType(ObservationDataType.BOOLEAN);
                break;
            case "DECIMAL":
                xsdDataElement.setDataType(ObservationDataType.DECIMAL);
                break;
            case "INTEGER":
                xsdDataElement.setDataType(ObservationDataType.INTEGER);
                break;
            case "DATE":
                xsdDataElement.setDataType(ObservationDataType.DATE);
                break;
            case "DATETIME":
                xsdDataElement.setDataType(ObservationDataType.DATETIME);
                break;
            case "TIME":
                xsdDataElement.setDataType(ObservationDataType.TIME);
                break;
            case "STRING":
                xsdDataElement.setDataType(ObservationDataType.STRING);
                break;
            default:
                xsdDataElement.setValueTypeIdentifier(elementSchemaTypeName);
                Map<String, String> valueRestrictions = readSimpleTypeRestrictionFacets(elementSchemaType);
                if (valueRestrictions == null || valueRestrictions.isEmpty())
                    xsdDataElement.setDataType(ObservationDataType.VALUEQUANTITY);
                else {
                    xsdDataElement.setDataType(ObservationDataType.VALUECODING);
                    xsdDataElement.setCodingRestrictions(valueRestrictions);
                }
            }
        }
        return xsdDataElement;
    }

    private String readAnnotatedElementDocumentation(XmlSchemaAnnotated annotatedElement) throws Exception {
        List<XmlSchemaAnnotationItem> annotationItems = annotatedElement.getAnnotation().getItems();
        if (!annotationItems.isEmpty()) {
            XmlSchemaDocumentation documentation = (XmlSchemaDocumentation) annotationItems.get(0);
            return documentation.getMarkup().item(0).getTextContent();
        } else {
            throw new Exception("Unable to read documentation for annotatedElement: " + annotatedElement.toString());
        }
    }

    private Map<String, String> readSimpleTypeRestrictionFacets(XmlSchemaSimpleType simpleType) throws Exception {
        Map<String, String> facets = new HashMap<>();
        XmlSchemaSimpleTypeRestriction simpleTypeRestriction = (XmlSchemaSimpleTypeRestriction) simpleType.getContent();
        for (XmlSchemaFacet facet : simpleTypeRestriction.getFacets()) {
            String facetValue = facet.getValue().toString();
            String facetDocumentation = readAnnotatedElementDocumentation(facet);
            facets.put(facetValue, facetDocumentation);
        }
        return facets;
    }

    public Map<DicaRegistration, XsdDictionary> getXsdDictionaries() {
        return xsdDictionaries;
    }

    public void setXsdDictionaries(Map<DicaRegistration, XsdDictionary> xsdDictionaries) {
        this.xsdDictionaries = xsdDictionaries;
    }
}
