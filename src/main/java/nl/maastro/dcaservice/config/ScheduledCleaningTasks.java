package nl.maastro.dcaservice.config;

import nl.maastro.dcaservice.domain.FhirTransactionLog;
import nl.maastro.dcaservice.repository.FhirTransactionLogRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Configuration
@EnableScheduling
public class ScheduledCleaningTasks {

	private final static Logger logger = Logger.getLogger(ScheduledCleaningTasks.class);

	@Value("${fhirtransactionlog.daysofhistory:60}")
	private int historyDays;


	@Autowired
    private FhirTransactionLogRepository fhirTransactionLogRepository;

	//sec min hour
	@Scheduled(cron="0 */10 * * * ?")
	public void cleanFhirTransactionLogRepository() {
		logger.info("Scheduled FhirTransactionLogRepository Cleaning Task");
        Instant instant = Instant.now();
        instant = instant.minus(historyDays, ChronoUnit.DAYS);
        cleanRepository(instant);
	}

	private void cleanRepository(Instant instant){
		List<FhirTransactionLog> logs = fhirTransactionLogRepository.findByDateLessThan(instant);
		logger.info("Cleaning nr of log entries: " + logs.size());
        fhirTransactionLogRepository.delete(logs);
	}

}
