package nl.maastro.dcaservice.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.IGenericClient;
import ca.uhn.fhir.rest.client.interceptor.BasicAuthInterceptor;
import nl.maastro.dcaservice.service.DicaRegistrationService;

@Configuration
@EnableConfigurationProperties(FhirConfigurationProperties.class)
public class FhirConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(DicaRegistrationService.class);
    private FhirContext fhirContext;

    @Autowired
    FhirConfigurationProperties fhirEndpointProperties;

    @Bean(name = "fhirContext")
    public FhirContext createFhirContext() {
        fhirContext = FhirContext.forDstu3(); 
        return fhirContext;
    }

    @Bean(name = "fhirClient")
    @DependsOn("fhirContext")
    public IGenericClient createFhirClient() {
        String address = fhirEndpointProperties.getAddress();
        String username = fhirEndpointProperties.getUsername();
        String password = fhirEndpointProperties.getPassword();
        IGenericClient client = fhirContext.newRestfulGenericClient(address);
        if (username != null && password != null) { 
            BasicAuthInterceptor authInterceptor = new BasicAuthInterceptor(username, password);
            client.registerInterceptor(authInterceptor);
        } else {
            logger.warn("No authentication credentials found for FHIR endpoint: " + address);
        }
        return client;
    }
}
