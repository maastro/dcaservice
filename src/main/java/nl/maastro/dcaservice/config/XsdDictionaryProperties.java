package nl.maastro.dcaservice.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix="dcaservice")
public class XsdDictionaryProperties {
	
	private Map<String, String> xsdDictionary = new HashMap<>();

	public Map<String, String> getXsdDictionary() {
		return xsdDictionary;
	}

	public void setXsdDictionary(Map<String, String> xsdDictionary) {
		this.xsdDictionary = xsdDictionary;
	}
}
