package nl.maastro.dcaservice.web.rest;

import com.codahale.metrics.annotation.Timed;
import nl.maastro.dcaservice.domain.PlanObservation;

import nl.maastro.dcaservice.repository.PlanObservationRepository;
import nl.maastro.dcaservice.web.rest.util.HeaderUtil;
import nl.maastro.dcaservice.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PlanObservation.
 */
@RestController
@RequestMapping("/api")
public class PlanObservationResource {

    private final Logger log = LoggerFactory.getLogger(PlanObservationResource.class);

    private static final String ENTITY_NAME = "planObservation";

    private final PlanObservationRepository planObservationRepository;

    public PlanObservationResource(PlanObservationRepository planObservationRepository) {
        this.planObservationRepository = planObservationRepository;
    }

    /**
     * POST  /plan-observations : Create a new planObservation.
     *
     * @param planObservation the planObservation to create
     * @return the ResponseEntity with status 201 (Created) and with body the new planObservation, or with status 400 (Bad Request) if the planObservation has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/plan-observations")
    @Timed
    public ResponseEntity<PlanObservation> createPlanObservation(@RequestBody PlanObservation planObservation) throws URISyntaxException {
        log.debug("REST request to save PlanObservation : {}", planObservation);
        if (planObservation.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new planObservation cannot already have an ID")).body(null);
        }
        PlanObservation result = planObservationRepository.save(planObservation);
        return ResponseEntity.created(new URI("/api/plan-observations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /plan-observations : Updates an existing planObservation.
     *
     * @param planObservation the planObservation to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated planObservation,
     * or with status 400 (Bad Request) if the planObservation is not valid,
     * or with status 500 (Internal Server Error) if the planObservation couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/plan-observations")
    @Timed
    public ResponseEntity<PlanObservation> updatePlanObservation(@RequestBody PlanObservation planObservation) throws URISyntaxException {
        log.debug("REST request to update PlanObservation : {}", planObservation);
        if (planObservation.getId() == null) {
            return createPlanObservation(planObservation);
        }
        PlanObservation result = planObservationRepository.save(planObservation);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, planObservation.getId().toString()))
            .body(result);
    }

    /**
     * GET  /plan-observations : get all the planObservations.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of planObservations in body
     */
    @GetMapping("/plan-observations")
    @Timed
    public ResponseEntity<List<PlanObservation>> getAllPlanObservations(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of PlanObservations");
        Page<PlanObservation> page = planObservationRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/plan-observations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /plan-observations/:id : get the "id" planObservation.
     *
     * @param id the id of the planObservation to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the planObservation, or with status 404 (Not Found)
     */
    @GetMapping("/plan-observations/{id}")
    @Timed
    public ResponseEntity<PlanObservation> getPlanObservation(@PathVariable Long id) {
        log.debug("REST request to get PlanObservation : {}", id);
        PlanObservation planObservation = planObservationRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(planObservation));
    }

    /**
     * DELETE  /plan-observations/:id : delete the "id" planObservation.
     *
     * @param id the id of the planObservation to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/plan-observations/{id}")
    @Timed
    public ResponseEntity<Void> deletePlanObservation(@PathVariable Long id) {
        log.debug("REST request to delete PlanObservation : {}", id);
        planObservationRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
