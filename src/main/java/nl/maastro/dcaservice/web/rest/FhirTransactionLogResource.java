package nl.maastro.dcaservice.web.rest;

import com.codahale.metrics.annotation.Timed;
import nl.maastro.dcaservice.domain.FhirTransactionLog;

import nl.maastro.dcaservice.repository.FhirTransactionLogRepository;
import nl.maastro.dcaservice.web.rest.util.HeaderUtil;
import nl.maastro.dcaservice.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing FhirTransactionLog.
 */
@RestController
@RequestMapping("/api")
public class FhirTransactionLogResource {

    private final Logger log = LoggerFactory.getLogger(FhirTransactionLogResource.class);

    private static final String ENTITY_NAME = "fhirTransactionLog";

    private final FhirTransactionLogRepository fhirTransactionLogRepository;

    public FhirTransactionLogResource(FhirTransactionLogRepository fhirTransactionLogRepository) {
        this.fhirTransactionLogRepository = fhirTransactionLogRepository;
    }

    /**
     * POST  /fhir-transaction-logs : Create a new fhirTransactionLog.
     *
     * @param fhirTransactionLog the fhirTransactionLog to create
     * @return the ResponseEntity with status 201 (Created) and with body the new fhirTransactionLog, or with status 400 (Bad Request) if the fhirTransactionLog has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/fhir-transaction-logs")
    @Timed
    public ResponseEntity<FhirTransactionLog> createFhirTransactionLog(@RequestBody FhirTransactionLog fhirTransactionLog) throws URISyntaxException {
        log.debug("REST request to save FhirTransactionLog : {}", fhirTransactionLog);
        if (fhirTransactionLog.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new fhirTransactionLog cannot already have an ID")).body(null);
        }
        FhirTransactionLog result = fhirTransactionLogRepository.save(fhirTransactionLog);
        return ResponseEntity.created(new URI("/api/fhir-transaction-logs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /fhir-transaction-logs : Updates an existing fhirTransactionLog.
     *
     * @param fhirTransactionLog the fhirTransactionLog to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated fhirTransactionLog,
     * or with status 400 (Bad Request) if the fhirTransactionLog is not valid,
     * or with status 500 (Internal Server Error) if the fhirTransactionLog couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/fhir-transaction-logs")
    @Timed
    public ResponseEntity<FhirTransactionLog> updateFhirTransactionLog(@RequestBody FhirTransactionLog fhirTransactionLog) throws URISyntaxException {
        log.debug("REST request to update FhirTransactionLog : {}", fhirTransactionLog);
        if (fhirTransactionLog.getId() == null) {
            return createFhirTransactionLog(fhirTransactionLog);
        }
        FhirTransactionLog result = fhirTransactionLogRepository.save(fhirTransactionLog);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, fhirTransactionLog.getId().toString()))
            .body(result);
    }

    /**
     * GET  /fhir-transaction-logs : get all the fhirTransactionLogs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of fhirTransactionLogs in body
     */
    @GetMapping("/fhir-transaction-logs")
    @Timed
    public ResponseEntity<List<FhirTransactionLog>> getAllFhirTransactionLogs(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of FhirTransactionLogs");
        Page<FhirTransactionLog> page = fhirTransactionLogRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/fhir-transaction-logs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /fhir-transaction-logs/:id : get the "id" fhirTransactionLog.
     *
     * @param id the id of the fhirTransactionLog to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the fhirTransactionLog, or with status 404 (Not Found)
     */
    @GetMapping("/fhir-transaction-logs/{id}")
    @Timed
    public ResponseEntity<FhirTransactionLog> getFhirTransactionLog(@PathVariable Long id) {
        log.debug("REST request to get FhirTransactionLog : {}", id);
        FhirTransactionLog fhirTransactionLog = fhirTransactionLogRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(fhirTransactionLog));
    }

    /**
     * DELETE  /fhir-transaction-logs/:id : delete the "id" fhirTransactionLog.
     *
     * @param id the id of the fhirTransactionLog to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/fhir-transaction-logs/{id}")
    @Timed
    public ResponseEntity<Void> deleteFhirTransactionLog(@PathVariable Long id) {
        log.debug("REST request to delete FhirTransactionLog : {}", id);
        fhirTransactionLogRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
