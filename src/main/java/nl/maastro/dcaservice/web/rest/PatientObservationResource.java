package nl.maastro.dcaservice.web.rest;

import com.codahale.metrics.annotation.Timed;
import nl.maastro.dcaservice.domain.PatientObservation;

import nl.maastro.dcaservice.repository.PatientObservationRepository;
import nl.maastro.dcaservice.web.rest.util.HeaderUtil;
import nl.maastro.dcaservice.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PatientObservation.
 */
@RestController
@RequestMapping("/api")
public class PatientObservationResource {

    private final Logger log = LoggerFactory.getLogger(PatientObservationResource.class);

    private static final String ENTITY_NAME = "patientObservation";

    private final PatientObservationRepository patientObservationRepository;

    public PatientObservationResource(PatientObservationRepository patientObservationRepository) {
        this.patientObservationRepository = patientObservationRepository;
    }

    /**
     * POST  /patient-observations : Create a new patientObservation.
     *
     * @param patientObservation the patientObservation to create
     * @return the ResponseEntity with status 201 (Created) and with body the new patientObservation, or with status 400 (Bad Request) if the patientObservation has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/patient-observations")
    @Timed
    public ResponseEntity<PatientObservation> createPatientObservation(@RequestBody PatientObservation patientObservation) throws URISyntaxException {
        log.debug("REST request to save PatientObservation : {}", patientObservation);
        if (patientObservation.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new patientObservation cannot already have an ID")).body(null);
        }
        PatientObservation result = patientObservationRepository.save(patientObservation);
        return ResponseEntity.created(new URI("/api/patient-observations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /patient-observations : Updates an existing patientObservation.
     *
     * @param patientObservation the patientObservation to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated patientObservation,
     * or with status 400 (Bad Request) if the patientObservation is not valid,
     * or with status 500 (Internal Server Error) if the patientObservation couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/patient-observations")
    @Timed
    public ResponseEntity<PatientObservation> updatePatientObservation(@RequestBody PatientObservation patientObservation) throws URISyntaxException {
        log.debug("REST request to update PatientObservation : {}", patientObservation);
        if (patientObservation.getId() == null) {
            return createPatientObservation(patientObservation);
        }
        PatientObservation result = patientObservationRepository.save(patientObservation);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, patientObservation.getId().toString()))
            .body(result);
    }

    /**
     * GET  /patient-observations : get all the patientObservations.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of patientObservations in body
     */
    @GetMapping("/patient-observations")
    @Timed
    public ResponseEntity<List<PatientObservation>> getAllPatientObservations(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of PatientObservations");
        Page<PatientObservation> page = patientObservationRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/patient-observations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /patient-observations/:id : get the "id" patientObservation.
     *
     * @param id the id of the patientObservation to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the patientObservation, or with status 404 (Not Found)
     */
    @GetMapping("/patient-observations/{id}")
    @Timed
    public ResponseEntity<PatientObservation> getPatientObservation(@PathVariable Long id) {
        log.debug("REST request to get PatientObservation : {}", id);
        PatientObservation patientObservation = patientObservationRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(patientObservation));
    }

    /**
     * DELETE  /patient-observations/:id : delete the "id" patientObservation.
     *
     * @param id the id of the patientObservation to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/patient-observations/{id}")
    @Timed
    public ResponseEntity<Void> deletePatientObservation(@PathVariable Long id) {
        log.debug("REST request to delete PatientObservation : {}", id);
        patientObservationRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
