package nl.maastro.dcaservice.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import nl.maastro.dcaservice.domain.RnvPlan;
import nl.maastro.dcaservice.domain.enumeration.DicaRegistration;
import nl.maastro.dcaservice.repository.RnvPlanRepository;
import nl.maastro.dcaservice.service.DlcaRegistrationService;
import nl.maastro.dcaservice.web.rest.util.HeaderUtil;
import nl.maastro.dcaservice.web.rest.util.PaginationUtil;

/**
 * REST controller for managing RnvPlan.
 */
@RestController
@RequestMapping("/api")
public class RnvPlanResource {

    private final Logger log = LoggerFactory.getLogger(RnvPlanResource.class);

    private static final String ENTITY_NAME = "rnvPlan";

    private static final String APPLICATION_NAME = "dcaserviceApp";

    private final RnvPlanRepository rnvPlanRepository;

    private final DlcaRegistrationService dlcaRegistrationService;

    public RnvPlanResource(RnvPlanRepository rnvPlanRepository, 
            DlcaRegistrationService dlcaRegistrationService) {
        this.rnvPlanRepository = rnvPlanRepository;
        this.dlcaRegistrationService = dlcaRegistrationService;
    }

    /**
     * POST  /rnv-plans : Create a new rnvPlan.
     *
     * @param rnvPlan the rnvPlan to create
     * @return the ResponseEntity with status 201 (Created) and with body the new rnvPlan, or with status 400 (Bad Request) if the rnvPlan has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/rnv-plans")
    @Timed
    public ResponseEntity<RnvPlan> createRnvPlan(@RequestBody RnvPlan rnvPlan) throws URISyntaxException {
        log.debug("REST request to save RnvPlan : {}", rnvPlan);
        if (rnvPlan.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new rnvPlan cannot already have an ID")).body(null);
        }
        RnvPlan result = rnvPlanRepository.save(rnvPlan);
        return ResponseEntity.created(new URI("/api/rnv-plans/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /rnv-plans : Updates an existing rnvPlan.
     *
     * @param rnvPlan the rnvPlan to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated rnvPlan,
     * or with status 400 (Bad Request) if the rnvPlan is not valid,
     * or with status 500 (Internal Server Error) if the rnvPlan couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/rnv-plans")
    @Timed
    public ResponseEntity<RnvPlan> updateRnvPlan(@RequestBody RnvPlan rnvPlan) throws URISyntaxException {
        log.debug("REST request to update RnvPlan : {}", rnvPlan);
        if (rnvPlan.getId() == null) {
            return createRnvPlan(rnvPlan);
        }
        RnvPlan result = rnvPlanRepository.save(rnvPlan);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, rnvPlan.getId().toString()))
            .body(result);
    }

    /**
     * GET  /rnv-plans : get all the rnvPlans.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of rnvPlans in body
     */
    @GetMapping("/rnv-plans")
    @Timed
    public ResponseEntity<List<RnvPlan>> getAllRnvPlans(@ApiParam Pageable pageable,
                                                        @RequestParam(required=false, defaultValue="false") boolean uncoupled) {
        log.debug("REST request to get a page of RnvPlans");
        Page<RnvPlan> page = null;
        if(!uncoupled){
            page = rnvPlanRepository.findAll(pageable);
        }
        else{
            page = rnvPlanRepository.findByPatient(null, pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/rnv-plans");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /rnv-plans : get all the rnvPlans.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of rnvPlans in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/rnv-plans/patientnull")
    @Timed
    public ResponseEntity<List<RnvPlan>> getAllRnvPlansUnmapped(@ApiParam Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of RnvPlans");
        Page<RnvPlan> page = rnvPlanRepository.findByPatient(null, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/rnv-plans/patientnull");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /rnv-plans/:id : get the "id" rnvPlan.
     *
     * @param id the id of the rnvPlan to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the rnvPlan, or with status 404 (Not Found)
     */
    @GetMapping("/rnv-plans/{id}")
    @Timed
    public ResponseEntity<RnvPlan> getRnvPlan(@PathVariable Long id) {
        log.debug("REST request to get RnvPlan : {}", id);
        RnvPlan rnvPlan = rnvPlanRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(rnvPlan));
    }
    
    /**
     * GET  /rnv-plans/fhir : fhir all rnvPlans.
     *
     * @return the ResponseEntity with status 200 (OK)
     */
    @GetMapping("/rnv-plans/fhir")
    @Timed
    @Transactional
    public ResponseEntity<Void> putAllRnvPlansOnFhir() {
        log.debug("REST request to send all RnvPlans");
        
        int numberOfSuccessfulTransactions = 0;
        int numberOfFailedTransactions = 0;
        int numberOfSkippedTransactions = 0;
        
        List<RnvPlan> rnvPlans = rnvPlanRepository.findAll();
        for (RnvPlan rnvPlan : rnvPlans) {
        	if (rnvPlan.getPatient() == null) {
        		log.warn("No patient coupled to RnvPlan: " + rnvPlan.getId()
        			+ "; RnvPlan is skipped");
        		numberOfSkippedTransactions += 1;
        		continue;
            }
        	try {
        		dlcaRegistrationService.putOnFhir(rnvPlan);
        		numberOfSuccessfulTransactions += 1;
        	} catch (Exception e) {
        		log.warn("Failed to send RnvPlan: " + rnvPlan.getId());
        		numberOfFailedTransactions += 1;
        	}
        }
        
        log.info("Processed request to send all RnvPlans"
                + "; number of successful transactions: " + numberOfSuccessfulTransactions
                + ", number of failed transactions: " + numberOfFailedTransactions
                + ", number of skipped transactions: " + numberOfSkippedTransactions);
        
        if (numberOfFailedTransactions == 0) {
            return ResponseEntity.status(HttpStatus.OK).build();
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "fhirAll.failed", 
                            "Request contains failed FHIR transactions"))
                    .build();
        }
    }

    /**
     * GET  /rnv-plans/:id/fhir : fhir the "id" rnvPlan.
     *
     * @param id the id of the rnvPlan to fhir
     * @return the ResponseEntity with status 200 (OK) and with body the rnvPlan, or with status 404 (Not Found)
     * @throws Exception
     */
    @GetMapping("/rnv-plans/{id}/fhir")
    @Timed
    @Transactional
    public ResponseEntity<Void> putRnvPlanOnFhir(@PathVariable Long id) throws Exception {
        log.debug("REST request to send RnvPlan : {}", id);
        
        // TODO: make dicaRegistration an input parameter
        DicaRegistration dicaRegistration = DicaRegistration.DLCA;
        
        try {
            RnvPlan rnvPlan = rnvPlanRepository.findOneWithEagerRelationships(id);
            if (rnvPlan == null) {
                throw new Exception("RnvPlan not found for id: " + id);
            }
            if (rnvPlan.getPatient() == null) {
                throw new Exception("No patient coupled to RnvPlan: " + rnvPlan);
            }
            if (!rnvPlan.getDicaRegistrations().stream().anyMatch(r -> r.getDicaRegistration().equals(dicaRegistration))) {
                throw new Exception("RnvPlan is not coupled to selected registration: " + dicaRegistration);
            }
            
            switch (dicaRegistration) {
            case DLCA:
                dlcaRegistrationService.putOnFhir(rnvPlan);
                break;
            default:
                throw new IllegalArgumentException("Unknown DICA registration: " + dicaRegistration);
            }
            return ResponseEntity.ok().headers(HeaderUtil.createAlert(APPLICATION_NAME + "."+ ENTITY_NAME +".fhir.sent", null)).build();
        } catch (Exception e) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createCustomFailureAlert(ENTITY_NAME, e.getMessage(), null)).body(null);
        }
     }

    /**
     * DELETE  /rnv-plans/:id : delete the "id" rnvPlan.
     *
     * @param id the id of the rnvPlan to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/rnv-plans/{id}")
    @Timed
    public ResponseEntity<Void> deleteRnvPlan(@PathVariable Long id) {
        log.debug("REST request to delete RnvPlan : {}", id);
        rnvPlanRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
