package nl.maastro.dcaservice.web.rest;

import com.codahale.metrics.annotation.Timed;
import nl.maastro.dcaservice.domain.DicaRegistrationEntity;

import nl.maastro.dcaservice.repository.DicaRegistrationEntityRepository;
import nl.maastro.dcaservice.web.rest.util.HeaderUtil;
import nl.maastro.dcaservice.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DicaRegistrationEntity.
 */
@RestController
@RequestMapping("/api")
public class DicaRegistrationEntityResource {

    private final Logger log = LoggerFactory.getLogger(DicaRegistrationEntityResource.class);

    private static final String ENTITY_NAME = "dicaRegistrationEntity";

    private final DicaRegistrationEntityRepository dicaRegistrationEntityRepository;

    public DicaRegistrationEntityResource(DicaRegistrationEntityRepository dicaRegistrationEntityRepository) {
        this.dicaRegistrationEntityRepository = dicaRegistrationEntityRepository;
    }

    /**
     * POST  /dica-registration-entities : Create a new dicaRegistrationEntity.
     *
     * @param dicaRegistrationEntity the dicaRegistrationEntity to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dicaRegistrationEntity, or with status 400 (Bad Request) if the dicaRegistrationEntity has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/dica-registration-entities")
    @Timed
    public ResponseEntity<DicaRegistrationEntity> createDicaRegistrationEntity(@RequestBody DicaRegistrationEntity dicaRegistrationEntity) throws URISyntaxException {
        log.debug("REST request to save DicaRegistrationEntity : {}", dicaRegistrationEntity);
        if (dicaRegistrationEntity.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new dicaRegistrationEntity cannot already have an ID")).body(null);
        }
        DicaRegistrationEntity result = dicaRegistrationEntityRepository.save(dicaRegistrationEntity);
        return ResponseEntity.created(new URI("/api/dica-registration-entities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /dica-registration-entities : Updates an existing dicaRegistrationEntity.
     *
     * @param dicaRegistrationEntity the dicaRegistrationEntity to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dicaRegistrationEntity,
     * or with status 400 (Bad Request) if the dicaRegistrationEntity is not valid,
     * or with status 500 (Internal Server Error) if the dicaRegistrationEntity couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/dica-registration-entities")
    @Timed
    public ResponseEntity<DicaRegistrationEntity> updateDicaRegistrationEntity(@RequestBody DicaRegistrationEntity dicaRegistrationEntity) throws URISyntaxException {
        log.debug("REST request to update DicaRegistrationEntity : {}", dicaRegistrationEntity);
        if (dicaRegistrationEntity.getId() == null) {
            return createDicaRegistrationEntity(dicaRegistrationEntity);
        }
        DicaRegistrationEntity result = dicaRegistrationEntityRepository.save(dicaRegistrationEntity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dicaRegistrationEntity.getId().toString()))
            .body(result);
    }

    /**
     * GET  /dica-registration-entities : get all the dicaRegistrationEntities.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of dicaRegistrationEntities in body
     */
    @GetMapping("/dica-registration-entities")
    @Timed
    public ResponseEntity<List<DicaRegistrationEntity>> getAllDicaRegistrationEntities(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of DicaRegistrationEntities");
        Page<DicaRegistrationEntity> page = dicaRegistrationEntityRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/dica-registration-entities");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /dica-registration-entities/:id : get the "id" dicaRegistrationEntity.
     *
     * @param id the id of the dicaRegistrationEntity to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dicaRegistrationEntity, or with status 404 (Not Found)
     */
    @GetMapping("/dica-registration-entities/{id}")
    @Timed
    public ResponseEntity<DicaRegistrationEntity> getDicaRegistrationEntity(@PathVariable Long id) {
        log.debug("REST request to get DicaRegistrationEntity : {}", id);
        DicaRegistrationEntity dicaRegistrationEntity = dicaRegistrationEntityRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dicaRegistrationEntity));
    }

    /**
     * DELETE  /dica-registration-entities/:id : delete the "id" dicaRegistrationEntity.
     *
     * @param id the id of the dicaRegistrationEntity to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/dica-registration-entities/{id}")
    @Timed
    public ResponseEntity<Void> deleteDicaRegistrationEntity(@PathVariable Long id) {
        log.debug("REST request to delete DicaRegistrationEntity : {}", id);
        dicaRegistrationEntityRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
