package nl.maastro.dcaservice.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.Set;

import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Coding;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.DateType;
import org.hl7.fhir.dstu3.model.DecimalType;
import org.hl7.fhir.dstu3.model.Enumerations.AdministrativeGender;
import org.hl7.fhir.dstu3.model.EpisodeOfCare;
import org.hl7.fhir.dstu3.model.Identifier.IdentifierUse;
import org.hl7.fhir.dstu3.model.IntegerType;
import org.hl7.fhir.dstu3.model.Meta;
import org.hl7.fhir.dstu3.model.Organization;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.Procedure;
import org.hl7.fhir.dstu3.model.Procedure.ProcedureStatus;
import org.hl7.fhir.dstu3.model.Quantity;
import org.hl7.fhir.dstu3.model.QuestionnaireResponse;
import org.hl7.fhir.dstu3.model.QuestionnaireResponse.QuestionnaireResponseItemComponent;
import org.hl7.fhir.dstu3.model.QuestionnaireResponse.QuestionnaireResponseStatus;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.StringType;
import org.hl7.fhir.dstu3.model.TimeType;
import org.hl7.fhir.dstu3.model.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import nl.maastro.dcaservice.domain.PatientObservation;
import nl.maastro.dcaservice.domain.PlanObservation;
import nl.maastro.dcaservice.domain.RnvPlan;
import nl.maastro.dcaservice.domain.XsdDataElement;
import nl.maastro.dcaservice.domain.XsdDictionary;
import nl.maastro.dcaservice.domain.constants.ObservationIdentifiers;
import nl.maastro.dcaservice.domain.constants.RtProcedureCodings;
import nl.maastro.dcaservice.repository.PatientObservationRepository;

/**
 * Service class for creating FHIR resources
 */
@Service
public class FhirResourceService {

    private static final Logger logger = LoggerFactory.getLogger(FhirResourceService.class);

    private static final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");

    private static final String ORGANIZATION_PROFILE = "http://mrdm.nl/fhir/StructureDefinition/nl-mrdm-organization";
    private static final String PATIENT_PROFILE = "http://mrdm.nl/fhir/StructureDefinition/nl-mrdm-patient";
    private static final String EPISODE_OF_CARE_PROFILE = "http://mrdm.nl/fhir/StructureDefinition/nl-mrdm-episodeofcare";
    private static final String RT_PROCEDURE_PROFILE = "http://mrdm.nl/fhir/StructureDefinition/mrdm-radiotherapy-procedure";
    private static final String RT_PLANNING_PROCEDURE_PROFILE = "http://mrdm.nl/fhir/StructureDefinition/mrdm-radiotherapy-treatmentplanning-procedure";
    private static final String RT_PLANNING_QR_PROFILE = "http://mrdm.nl/fhir/StructureDefinition/mrdm-radiotherapy-treatmentplanning-questionnaireresponse";
    
    private static final String AGB_SYSTEM = "http://fhir.nl/fhir/NamingSystem/agb-z";
    private static final String BSN_SYSTEM = "http://fhir.nl/fhir/NamingSystem/bsn";
    private static final String PATIENT_UPN_SYSTEM = "http://www.mrdm.nl/fhir/NamingSystem/patient_upn";
    private static final String MRDM_PROJECT_SYSTEM = "http://www.mrdm.nl/fhir/NamingSystem/project";

    private static final String MRDM_PROJECT = "radiotherapy";
    private static final String RT_PLANNING_QR_DISPLAY = "radiotherapy-planning-treatment-questionnaire";

    private PatientObservationRepository patientObservationRepository;
    
    @Value("${dcaservice.organization.name}")
    private String organizationName;

    @Value("${dcaservice.organization.agb-identifier}")
    private String organizationIdentifierAgb;

    @Value("${dcaservice.organization.dica-identifier}")
    private String organizationIdentifierDica;

    public FhirResourceService(PatientObservationRepository patientObservationRepository) {
        this.patientObservationRepository = patientObservationRepository;
    }
    
    public Organization createOrganization(Date currentDate, XsdDictionary xsdDictionary) {

        Organization organization = new Organization();
        organization.setId(organizationName + "-" + organizationIdentifierAgb);
        organization.setMeta(new Meta().setLastUpdated(currentDate).addProfile(ORGANIZATION_PROFILE));
        organization.addIdentifier()
            .setUse(IdentifierUse.OFFICIAL)
            .setSystem(AGB_SYSTEM)
            .setValue(organizationIdentifierAgb);
        organization.addIdentifier()
            .setUse(IdentifierUse.SECONDARY)
            .setSystem(xsdDictionary.getProjectUri() + "/" + "behandelzkhid_type")
            .setValue(organizationIdentifierDica);
        return organization;
    }


    public org.hl7.fhir.dstu3.model.Patient createPatient(
            nl.maastro.dcaservice.domain.Patient patientEntity,
            Organization organization,
            Date currentDate) throws Exception {

        org.hl7.fhir.dstu3.model.Patient patient = new org.hl7.fhir.dstu3.model.Patient();
        patient.setId(organizationName + "-" + patientEntity.getPatientUid());
        patient.setMeta(new Meta().setLastUpdated(currentDate).addProfile(PATIENT_PROFILE));

        patient.addIdentifier()
            .setUse(IdentifierUse.OFFICIAL)
            .setSystem(BSN_SYSTEM)
            .setValue(patientEntity.getBsn());
        patient.addIdentifier()
            .setUse(IdentifierUse.OFFICIAL)
            .setSystem(PATIENT_UPN_SYSTEM)
            .setValue(patientEntity.getPatientUid())
            .setAssigner(new Reference(organization));

        patient.addName()
            .setFamily(patientEntity.getNameFamily())
            .addPrefix(patientEntity.getNameFamilyPrefix())
            .addGiven(patientEntity.getNameInitials());

        switch (patientEntity.getGender()) {
        case MALE:
            patient.setGender(AdministrativeGender.MALE);
            break;
        case FEMALE:
            patient.setGender(AdministrativeGender.FEMALE);
            break;
        case OTHER:
        default:
            patient.setGender(AdministrativeGender.OTHER);
            break;
        }

        LocalDate birthDateLocal = patientEntity.getDateOfBirth();
        Date birthDate = Date.from(birthDateLocal.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        patient.setBirthDate(birthDate);

        Set<PatientObservation> deceaseDateObservations = patientObservationRepository
            .findDistinctPatientObservationsByPatientAndObservationIdentifier(
                patientEntity,
                ObservationIdentifiers.DECEASE_DATE);
        if (!deceaseDateObservations.isEmpty()) {
            PatientObservation deceaseDateObservation = deceaseDateObservations.iterator().next();
            try {
                Date deceaseDate = mapDateObservationToDate(deceaseDateObservation.getObservationValue());
                patient.setDeceased(new DateTimeType(deceaseDate));
            } catch (ParseException e) {
                throw new Exception("Unable to set decease date from deceaseDataObservation: "
                    + deceaseDateObservation.toString(), e);
            }
        }
        patient.setManagingOrganization(new Reference(organization));

        return patient;
    }
    
    public EpisodeOfCare createEpisodeOfCare(
            String studyInstanceUid,
            org.hl7.fhir.dstu3.model.Patient patientResource,
            Organization organizationResource,
            Date currentDate) {
        
        EpisodeOfCare episodeOfCare = new EpisodeOfCare();
        episodeOfCare.setId(studyInstanceUid);
        episodeOfCare.setMeta(new Meta().setLastUpdated(currentDate).addProfile(EPISODE_OF_CARE_PROFILE));
        episodeOfCare.addIdentifier()
            .setUse(IdentifierUse.SECONDARY)
            .setSystem(MRDM_PROJECT_SYSTEM)
            .setValue(MRDM_PROJECT);
        episodeOfCare.setPatient(new Reference(patientResource));
        episodeOfCare.setManagingOrganization(new Reference(organizationResource));
        return episodeOfCare;
    }

    public Procedure createRtProcedure(
            RnvPlan rnvPlan,
            org.hl7.fhir.dstu3.model.Patient patientResource,
            Organization organizationResource,
            EpisodeOfCare episodeOfCare,
            Date currentDate) {

        Procedure rtProcedure = new Procedure();
        rtProcedure.setId(rnvPlan.getStudyInstanceUid());
        rtProcedure.setMeta(new Meta().setLastUpdated(currentDate).addProfile(RT_PROCEDURE_PROFILE));
        rtProcedure.setSubject(new Reference(patientResource));
        rtProcedure.setContext(new Reference(episodeOfCare));
        
        rtProcedure.setCode(new CodeableConcept()
                .addCoding(RtProcedureCodings.EXTERNAL_BEAM_RADIATION_THERAPY));
        
        nl.maastro.dcaservice.domain.Patient patientEntity = rnvPlan.getPatient();
        ProcedureStatus rtProcedureStatus = determineRtProcedureStatus(patientEntity);
        rtProcedure.setStatus(rtProcedureStatus);
        Period rtProcedurePeriod = determineRtProcedurePeriod(patientEntity);
        rtProcedure.setPerformed(rtProcedurePeriod);
        rtProcedure.addPerformer().setActor(new Reference(organizationResource));

        return rtProcedure;
    }

    public Procedure createRtPlanningTreatmentProcedure(
            RnvPlan rnvPlan,
            org.hl7.fhir.dstu3.model.Patient patientResource,
            Organization organizationResource,
            EpisodeOfCare episodeOfCare,
            Date currentDate) {

        Procedure rtPlanningProcedure = new Procedure();
        rtPlanningProcedure.setId(rnvPlan.getPlanSopInstanceUid());
        rtPlanningProcedure.setMeta(new Meta().setLastUpdated(currentDate).addProfile(RT_PLANNING_PROCEDURE_PROFILE));
        rtPlanningProcedure.setStatus(ProcedureStatus.COMPLETED);
        rtPlanningProcedure.setSubject(new Reference(patientResource));
        rtPlanningProcedure.setContext(new Reference(episodeOfCare));

        LocalDate localDate = rnvPlan.getDateCreated();
        Date date = Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        rtPlanningProcedure.setPerformed(new Period().setStart(date).setEnd(date));

        rtPlanningProcedure.addPerformer().setActor(new Reference(organizationResource));

        return rtPlanningProcedure;
    }

    public QuestionnaireResponse createRtPlanningTreatmentQuestionnaireResponse(
            RnvPlan rnvPlan,
            Procedure rtPlanningProcedure,
            EpisodeOfCare episodeOfCare,
            Date currentDate,
            XsdDictionary xsdDictionary) throws Exception {

        QuestionnaireResponse qr = new QuestionnaireResponse();
        qr.setMeta(new Meta().setLastUpdated(currentDate).addProfile(RT_PLANNING_QR_PROFILE));
        qr.setId(rnvPlan.getPlanSopInstanceUid());

        qr.setQuestionnaire(new Reference().setDisplay(RT_PLANNING_QR_DISPLAY));
        qr.setStatus(QuestionnaireResponseStatus.COMPLETED);
        qr.setSubject(new Reference(rtPlanningProcedure));
        qr.setContext(new Reference(episodeOfCare));

        QuestionnaireResponseItemComponent qrItem = new QuestionnaireResponseItemComponent();
        qrItem.setId(xsdDictionary.getProjectName());
        qrItem.setLinkId(xsdDictionary.getProjectVersion());
        for (PlanObservation observation : rnvPlan.getPlanObservations()) {
            String observationIdentifier = observation.getObservationIdentifier();
            XsdDataElement xsdDataElement = xsdDictionary.getDataDictionary().get(observationIdentifier);

            // Skip if observation is not in XSD dictionary
            if (xsdDataElement != null) {
                try {
                    Type value = mapObservationValue(xsdDictionary, xsdDataElement, observation.getObservationValue());
                    QuestionnaireResponseItemComponent qrSubitem = new QuestionnaireResponseItemComponent()
                            .setLinkId("behandeling/" + observationIdentifier)
                            .setText(xsdDataElement.getDescription());
                    qrSubitem.addAnswer().setValue(value);
                    qrItem.addItem(qrSubitem);
                } catch (Exception e) {
                    throw new Exception("Unable to create QuestionnaireResponseItem for observation="
                            + observation.toString(), e);
                }
            }
        }

        qr.addItem(qrItem);
        return qr;
    }

    private Period determineRtProcedurePeriod(nl.maastro.dcaservice.domain.Patient patientEntity) {
        Period rtProcedurePeriod = new Period();

        Set<PatientObservation> startDateObservations = patientObservationRepository
                .findDistinctPatientObservationsByPatientAndObservationIdentifier(
                        patientEntity,
                        ObservationIdentifiers.START_THORAX);
        try {
            PatientObservation startDateObservation = startDateObservations.iterator().next();
            Date startDate = mapDateObservationToDate(startDateObservation.getObservationValue());
            rtProcedurePeriod.setStart(startDate);
        } catch (NoSuchElementException | ParseException e) {
            logger.warn("Unable to set start date of RT-Procedure", e);
        }

        Set<PatientObservation> endDateObservations = patientObservationRepository
                .findDistinctPatientObservationsByPatientAndObservationIdentifier(
                        patientEntity,
                        ObservationIdentifiers.END_THORAX);
        try {
            PatientObservation endDateObservation = endDateObservations.iterator().next();
            Date endDate = mapDateObservationToDate(endDateObservation.getObservationValue());
            rtProcedurePeriod.setEnd(endDate);
        } catch (NoSuchElementException | ParseException e) {
            logger.warn("Unable to set end date of RT-Procedure", e);
        }

        return rtProcedurePeriod;
    }

    private ProcedureStatus determineRtProcedureStatus(nl.maastro.dcaservice.domain.Patient patientEntity) {
        // TODO: determine the overall status of the rt-procedure
        // The current implementation always returns the status INPROGRESS
        ProcedureStatus rtProcedureStatus = ProcedureStatus.INPROGRESS;
        return rtProcedureStatus;
    }

    private Date mapDateObservationToDate(String dateString) throws ParseException {
        return dateFormatter.parse(dateString);
    }

    private Type mapObservationValue(XsdDictionary xsdDictionary, 
            XsdDataElement xsdDataElement, String value) throws Exception {
        switch (xsdDataElement.getDataType()) {
        case BOOLEAN:
            return new BooleanType(value);
        case DECIMAL:
            return new DecimalType(value);
        case INTEGER:
            return new IntegerType(value);
        case DATE:
            return new DateType(value);
        case DATETIME:
            return new DateTimeType(value);
        case TIME:
            return new TimeType(value);
        case STRING:
            return new StringType(value);
        case VALUEQUANTITY:
            return new Quantity().setValue(Double.valueOf(value));
        case VALUECODING:
            return new Coding()
                    .setSystem(xsdDictionary.getProjectUri() + "/" + xsdDataElement.getValueTypeIdentifier())
                    .setCode(value)
                    .setDisplay(xsdDataElement.getCodingRestrictions().get(value));
        default:
            throw new Exception("Unknown data type:" + xsdDataElement.getDataType());
        }
    }
}
