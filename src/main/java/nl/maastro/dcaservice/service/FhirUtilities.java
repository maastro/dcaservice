package nl.maastro.dcaservice.service;

import java.util.Set;

import org.hl7.fhir.dstu3.model.BaseResource;
import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.dstu3.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.dstu3.model.Bundle.BundleType;
import org.hl7.fhir.dstu3.model.Bundle.HTTPVerb;
import org.hl7.fhir.dstu3.model.Resource;
import org.springframework.http.HttpStatus;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.IGenericClient;

/**
 * Utility class for common FHIR operations
 */
public class FhirUtilities {

    public static Bundle createBundle(BundleType bundleType, Set<Resource> resources) {
        Bundle bundle = new Bundle();
        bundle.setType(bundleType);
        for (Resource resource : resources) {
            bundle = addResourceToBundle(bundle, resource, HTTPVerb.PUT);
        }
        return bundle;
    }

    public static Bundle addResourceToBundle(Bundle bundle, Resource resource, HTTPVerb method) {
        String url = resource.getClass().getSimpleName() + "/" + resource.getId();
        bundle.addEntry()
            .setResource(resource)
            .getRequest()
            .setUrl(url)
            .setMethod(method);
        return bundle;
    }

    public static Bundle sendBundle(IGenericClient fhirClient, Bundle bundle) {
        Bundle response = fhirClient.transaction().withBundle(bundle).execute();
        return response;
    }

    public static String encodeResourceToJson(FhirContext fhirContext, BaseResource resource) {
        return fhirContext.newJsonParser().setPrettyPrint(true).encodeResourceToString(resource);
    }

    public static String encodeResourceToXml(FhirContext fhirContext, BaseResource resource) {
        return fhirContext.newXmlParser().setPrettyPrint(true).encodeResourceToString(resource);
    }

    public static boolean has2xxResponseStatus(BundleEntryComponent entry) {
        String status = entry.getResponse().getStatus();
        int statusCode = Integer.valueOf(status.substring(0, 3));
        HttpStatus httpStatus = HttpStatus.valueOf(statusCode);
        return httpStatus.is2xxSuccessful();
    }
}
