package nl.maastro.dcaservice.service;

import java.time.Instant;
import java.util.NoSuchElementException;

import javax.annotation.PostConstruct;

import org.hl7.fhir.dstu3.model.Bundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ca.uhn.fhir.rest.client.IGenericClient;
import nl.maastro.dcaservice.config.XsdDictionaryConfiguration;
import nl.maastro.dcaservice.domain.FhirTransactionLog;
import nl.maastro.dcaservice.domain.RnvPlan;
import nl.maastro.dcaservice.domain.XsdDictionary;
import nl.maastro.dcaservice.domain.enumeration.DicaRegistration;
import nl.maastro.dcaservice.domain.enumeration.TransactionStatus;
import nl.maastro.dcaservice.repository.FhirTransactionLogRepository;

@Service
public abstract class DicaRegistrationService {

    private static final Logger logger = LoggerFactory.getLogger(DicaRegistrationService.class);
    private FhirTransactionLogRepository fhirTransactionLogRepository;
    
    private XsdDictionaryConfiguration xsdDictionaryConfiguration;
    
    protected XsdDictionary xsdDictionary;
    protected IGenericClient fhirClient;
    
    public abstract DicaRegistration getDicaRegistration();

    protected abstract Bundle createFhirBundle(RnvPlan rnvPlan) throws Exception;
    
    public DicaRegistrationService(IGenericClient fhirClient,
            FhirTransactionLogRepository fhirTransactionLogRepository,
            XsdDictionaryConfiguration xsdDictionaryConfiguration) {
        this.fhirClient = fhirClient;
        this.fhirTransactionLogRepository = fhirTransactionLogRepository;
        this.xsdDictionaryConfiguration = xsdDictionaryConfiguration;
    }
    
    @PostConstruct
    private void initialize() throws NoSuchElementException {
        DicaRegistration dicaRegistration = getDicaRegistration();
        xsdDictionary = xsdDictionaryConfiguration.getXsdDictionaries().get(dicaRegistration);
        if (xsdDictionary == null) {
            throw new NoSuchElementException("XSD dictionary not found for dicaRegistration=" + dicaRegistration);
        }
    }
    
    public void putOnFhir(RnvPlan rnvPlan) throws Exception {
        FhirTransactionLog fhirTransactionLog = new FhirTransactionLog();
        fhirTransactionLog.setDate(Instant.now());
        fhirTransactionLog.setPlanUid(rnvPlan.getPlanSopInstanceUid());
        
        try {
            Bundle outgoingBundle = this.createFhirBundle(rnvPlan);
            String outgoingBundleXml = FhirUtilities.encodeResourceToXml(fhirClient.getFhirContext(), outgoingBundle);
            fhirTransactionLog.setOutgoingBundle(outgoingBundleXml);
            logger.debug("Outgoing bundle: " + outgoingBundleXml);
            
            Bundle responseBundle = this.sendFhirBundle(outgoingBundle);
            String responseBundleXml = FhirUtilities.encodeResourceToXml(fhirClient.getFhirContext(), responseBundle);
            fhirTransactionLog.setReponseBundle(responseBundleXml);
            logger.debug("Response bundle: " + responseBundleXml);
            
            if (!responseBundle.getEntry().stream().allMatch(e -> FhirUtilities.has2xxResponseStatus(e))) {
                throw new Exception("Response bundle contains failed transactions (non-2xx http status code)");
            }
            
            fhirTransactionLog.setStatus(TransactionStatus.SUCCESS);
            logger.info("Successfully sent FHIR bundle for RnvPlan: " + rnvPlan);
            
        } catch (Exception e) {
            fhirTransactionLog.setErrorMessage(e.getMessage());
            fhirTransactionLog.setStatus(TransactionStatus.FAILED);
            fhirTransactionLogRepository.save(fhirTransactionLog);
            logger.error("Failed to send FHIR bundle for RnvPlan: " + rnvPlan, e);
            throw e;
        } finally {
            fhirTransactionLogRepository.save(fhirTransactionLog);
        }
    }
    
    public Bundle sendFhirBundle(Bundle bundle) throws Exception {
        try {
            Bundle responseBundle = fhirClient.transaction().withBundle(bundle).execute();
            return responseBundle;
        } catch (Exception e) {
            throw new Exception("FHIR transacation failed", e);
        }
    }
}
