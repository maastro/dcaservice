package nl.maastro.dcaservice.service;

import java.util.Date;

import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.dstu3.model.Bundle.BundleType;
import org.hl7.fhir.dstu3.model.Bundle.HTTPVerb;
import org.hl7.fhir.dstu3.model.EpisodeOfCare;
import org.hl7.fhir.dstu3.model.Organization;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Procedure;
import org.hl7.fhir.dstu3.model.QuestionnaireResponse;
import org.hl7.fhir.dstu3.model.Reference;
import org.springframework.stereotype.Service;

import ca.uhn.fhir.rest.client.IGenericClient;
import nl.maastro.dcaservice.config.XsdDictionaryConfiguration;
import nl.maastro.dcaservice.domain.RnvPlan;
import nl.maastro.dcaservice.domain.enumeration.DicaRegistration;
import nl.maastro.dcaservice.repository.FhirTransactionLogRepository;

@Service
public class DlcaRegistrationService extends DicaRegistrationService {

    private static DicaRegistration DICA_REGISTRATION = DicaRegistration.DLCA;
    private FhirResourceService fhirResourceService;
    
    public DlcaRegistrationService(IGenericClient fhirClient,
            FhirTransactionLogRepository fhirTransactionLogRepository,
            FhirResourceService fhirResourceService,
            XsdDictionaryConfiguration xsdDictionaryConfiguration) {
        super(fhirClient, fhirTransactionLogRepository, xsdDictionaryConfiguration);
        this.fhirResourceService = fhirResourceService;
    }
    
    public DicaRegistration getDicaRegistration() {
        return DICA_REGISTRATION;
    }

    public Bundle createFhirBundle(RnvPlan rnvPlan) throws Exception {
        Date currentDate = new Date();
        Organization organization = fhirResourceService.createOrganization(
                        currentDate, xsdDictionary);
        Patient patient = fhirResourceService.createPatient(
                rnvPlan.getPatient(), organization, currentDate);
        EpisodeOfCare episodeOfCare = fhirResourceService.createEpisodeOfCare(
                rnvPlan.getStudyInstanceUid(), patient, organization, currentDate);

        Procedure rtProcedure = fhirResourceService.createRtProcedure(
                rnvPlan, patient, organization, episodeOfCare, currentDate);

        Procedure rtPlanningProcedure = fhirResourceService.createRtPlanningTreatmentProcedure(
                rnvPlan, patient, organization, episodeOfCare, currentDate);
        rtPlanningProcedure.addPartOf(new Reference(rtProcedure));
        
        QuestionnaireResponse rtPlanningQuestionnaire = fhirResourceService.createRtPlanningTreatmentQuestionnaireResponse(
                rnvPlan, rtPlanningProcedure, episodeOfCare, currentDate, xsdDictionary);

        Bundle bundle = new Bundle();
        bundle.setType(BundleType.TRANSACTION);
        FhirUtilities.addResourceToBundle(bundle, organization, HTTPVerb.PUT);
        FhirUtilities.addResourceToBundle(bundle, patient, HTTPVerb.PUT);
        FhirUtilities.addResourceToBundle(bundle, episodeOfCare, HTTPVerb.PUT);
        FhirUtilities.addResourceToBundle(bundle, rtProcedure, HTTPVerb.PUT);
        FhirUtilities.addResourceToBundle(bundle, rtPlanningProcedure, HTTPVerb.PUT);
        FhirUtilities.addResourceToBundle(bundle, rtPlanningQuestionnaire, HTTPVerb.PUT);
        return bundle;
    }
}
