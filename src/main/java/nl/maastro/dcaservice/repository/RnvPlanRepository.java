package nl.maastro.dcaservice.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import nl.maastro.dcaservice.domain.Patient;
import nl.maastro.dcaservice.domain.RnvPlan;

/**
 * Spring Data JPA repository for the RnvPlan entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RnvPlanRepository extends JpaRepository<RnvPlan,Long> {

    @Query("select distinct rnv_plan from RnvPlan rnv_plan left join fetch rnv_plan.dicaRegistrations")
    List<RnvPlan> findAllWithEagerRelationships();

    @Query("select rnv_plan from RnvPlan rnv_plan left join fetch rnv_plan.dicaRegistrations where rnv_plan.id =:id")
    RnvPlan findOneWithEagerRelationships(@Param("id") Long id);

	Page<RnvPlan> findByPatient(Patient patient, Pageable pageable);

}
