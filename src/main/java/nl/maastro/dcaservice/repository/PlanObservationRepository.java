package nl.maastro.dcaservice.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import nl.maastro.dcaservice.domain.PlanObservation;
import nl.maastro.dcaservice.domain.RnvPlan;


/**
 * Spring Data JPA repository for the PlanObservation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PlanObservationRepository extends JpaRepository<PlanObservation,Long> {

    Set<PlanObservation> findDistinctPlanObservationsByRnvPlanAndObservationIdentifier(RnvPlan planEntity, String startThorax);
}
