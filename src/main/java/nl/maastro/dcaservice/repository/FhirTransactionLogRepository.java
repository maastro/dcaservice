package nl.maastro.dcaservice.repository;

import nl.maastro.dcaservice.domain.FhirTransactionLog;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.time.Instant;
import java.util.List;


/**
 * Spring Data JPA repository for the FhirTransactionLog entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FhirTransactionLogRepository extends JpaRepository<FhirTransactionLog,Long> {

    List<FhirTransactionLog> findByDateLessThan(Instant time);
}
