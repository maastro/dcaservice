package nl.maastro.dcaservice.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import nl.maastro.dcaservice.domain.Patient;
import nl.maastro.dcaservice.domain.PatientObservation;


/**
 * Spring Data JPA repository for the PatientObservation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PatientObservationRepository extends JpaRepository<PatientObservation,Long> {

    Set<PatientObservation> findDistinctPatientObservationsByPatientAndObservationIdentifier(Patient patient, String identifier);

}
