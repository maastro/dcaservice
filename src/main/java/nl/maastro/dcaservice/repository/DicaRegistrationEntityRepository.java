package nl.maastro.dcaservice.repository;

import nl.maastro.dcaservice.domain.DicaRegistrationEntity;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the DicaRegistrationEntity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DicaRegistrationEntityRepository extends JpaRepository<DicaRegistrationEntity,Long> {

}
