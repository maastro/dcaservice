package nl.maastro.dcaservice.fhir;

import java.time.LocalDate;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import nl.maastro.dcaservice.DcaserviceApp;
import nl.maastro.dcaservice.domain.DicaRegistrationEntity;
import nl.maastro.dcaservice.domain.Patient;
import nl.maastro.dcaservice.domain.PatientObservation;
import nl.maastro.dcaservice.domain.PlanObservation;
import nl.maastro.dcaservice.domain.RnvPlan;
import nl.maastro.dcaservice.domain.constants.ObservationIdentifiers;
import nl.maastro.dcaservice.domain.enumeration.DicaRegistration;
import nl.maastro.dcaservice.domain.enumeration.Gender;
import nl.maastro.dcaservice.repository.DicaRegistrationEntityRepository;
import nl.maastro.dcaservice.repository.PatientObservationRepository;
import nl.maastro.dcaservice.repository.PatientRepository;
import nl.maastro.dcaservice.repository.PlanObservationRepository;
import nl.maastro.dcaservice.repository.RnvPlanRepository;
import nl.maastro.dcaservice.service.DlcaRegistrationService;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DcaserviceApp.class)
public class DlcaTest {

    private static final String DATOVL_IDENTIFIER = ObservationIdentifiers.DECEASE_DATE;
    private static final String VMAT_IDENTIFIER = ObservationIdentifiers.VMAT;
    private static final String MLD_IDENTIFIER = ObservationIdentifiers.MEAN_LUNG_DOSE;
    private static final String MAXOES_IDENTIFIER = ObservationIdentifiers.MAX_OESOPHAGUS_DOSE;
    private static final String CUMDOSIS_IDENTIFIER = ObservationIdentifiers.CUMULATIVE_DOSE;
    private static final String GEPLCUMDOSIS_IDENTIFIER = ObservationIdentifiers.IS_PLANNED_CUMULATIVE_DOSE;

    @Value("${dcaservice.organization.name}")
    private String organizationName;

    @Value("${dcaservice.organization.agb-identifier}")
    private String organizationIdentifierAgb;

    @Value("${dcaservice.organization.dica-identifier}")
    private String organizationIdentifierDica;

    @Autowired
    private PatientObservationRepository patientObservationRepository;

    @Autowired
    private PlanObservationRepository planObservationRepository;

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private RnvPlanRepository rnvPlanRepository;

    @Autowired
    private DicaRegistrationEntityRepository dicaRegistrationEntityRepository;

    @Autowired
    private DlcaRegistrationService dlcaRegistrationService;

    @Test
    @Transactional
    public void test() throws Exception {
        RnvPlan rnvPlan = createDummyRnvPlan();
        rnvPlanRepository.saveAndFlush(rnvPlan);
        Patient patient = createDummyPatient();
        patient.addRnvPlan(rnvPlan);
        patientRepository.saveAndFlush(patient);
        dlcaRegistrationService.putOnFhir(rnvPlan);
    }

    private Patient createDummyPatient() {
        Patient patient = new Patient();
        patientRepository.saveAndFlush(patient);
        patient.setPatientUid("12345");
        patient.setBsn("987654321");
        patient.setNameInitials("J");
        patient.setNameFamily("Boer");
        patient.setNameFamilyPrefix("de");
        patient.setGender(Gender.MALE);
        patient.setDateOfBirth(LocalDate.of(1963, 11, 18));
        patient.addPatientObservations(patientObservationRepository.saveAndFlush(createDummyPatientObservationDatovl()));
        patientRepository.saveAndFlush(patient);
        return patient;
    }

    private RnvPlan createDummyRnvPlan() {
        RnvPlan rnvPlan = new RnvPlan();
        rnvPlanRepository.saveAndFlush(rnvPlan);
        rnvPlan.setPlanSopInstanceUid("1.3.6.1.4.1.32722.40.6.123456789");
        rnvPlan.setStudyInstanceUid("1.3.6.1.4.1.32722.40.6.987654321");
        rnvPlan.setDateCreated(LocalDate.of(2016, 12, 11));
        rnvPlan.addDicaRegistrations(dicaRegistrationEntityRepository
                .saveAndFlush(new DicaRegistrationEntity().dicaRegistration(DicaRegistration.DLCA)));
        rnvPlan.addPlanObservations(planObservationRepository.saveAndFlush(createDummyPlanObservationVmat()));
        rnvPlan.addPlanObservations(planObservationRepository.saveAndFlush(createDummyPlanObservationMld()));
        rnvPlan.addPlanObservations(planObservationRepository.saveAndFlush(createDummyPlanObservationMaxoes()));
        rnvPlan.addPlanObservations(planObservationRepository.saveAndFlush(createDummyPlanObservationCumdosis()));
        rnvPlan.addPlanObservations(planObservationRepository.saveAndFlush(createDummyPlanObservationGeplcumdosis()));
        return rnvPlan;
    }

    private PatientObservation createDummyPatientObservationDatovl() {
        PatientObservation datovlObservation = new PatientObservation();
        datovlObservation.setObservationIdentifier(DATOVL_IDENTIFIER);
        datovlObservation.setObservationValue("2017-01-01");
        return datovlObservation;
    }

    private PlanObservation createDummyPlanObservationVmat() {
        PlanObservation vmatObservation = new PlanObservation();
        vmatObservation.setObservationIdentifier(VMAT_IDENTIFIER);
        vmatObservation.setObservationValue("1");
        return vmatObservation;
    }

    private PlanObservation createDummyPlanObservationMld() {
        PlanObservation mldObservation = new PlanObservation();
        mldObservation.setObservationIdentifier(MLD_IDENTIFIER);
        mldObservation.setObservationValue("8.0");
        return mldObservation;
    }

    private PlanObservation createDummyPlanObservationMaxoes() {
        PlanObservation maxoesObservation = new PlanObservation();
        maxoesObservation.setObservationIdentifier(MAXOES_IDENTIFIER);
        maxoesObservation.setObservationValue("15.0");
        return maxoesObservation;
    }

    private PlanObservation createDummyPlanObservationCumdosis() {
        PlanObservation cumdosisObservation = new PlanObservation();
        cumdosisObservation.setObservationIdentifier(CUMDOSIS_IDENTIFIER);
        cumdosisObservation.setObservationValue("60.0");
        return cumdosisObservation;
    }

    private PlanObservation createDummyPlanObservationGeplcumdosis() {
        PlanObservation geplcumdosisObservation = new PlanObservation();
        geplcumdosisObservation.setObservationIdentifier(GEPLCUMDOSIS_IDENTIFIER);
        geplcumdosisObservation.setObservationValue("1");
        return geplcumdosisObservation;
    }
}
