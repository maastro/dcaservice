package nl.maastro.dcaservice.fhir;

import static org.junit.Assert.assertTrue;

import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.dstu3.model.Bundle.BundleType;
import org.hl7.fhir.dstu3.model.Bundle.HTTPVerb;
import org.hl7.fhir.dstu3.model.Enumerations.AdministrativeGender;
import org.hl7.fhir.dstu3.model.Observation;
import org.hl7.fhir.dstu3.model.Observation.ObservationStatus;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Quantity;
import org.hl7.fhir.dstu3.model.Reference;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.rest.client.IGenericClient;
import nl.maastro.dcaservice.DcaserviceApp;
import nl.maastro.dcaservice.service.FhirUtilities;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DcaserviceApp.class)
public class GeneralFhirTest {
	
	private static final Logger logger = LoggerFactory.getLogger(GeneralFhirTest.class);
	private static final String FHIR_TEST_ENDPOINT = "http://fhirtest.uhn.ca/baseDstu3";
	private IGenericClient fhirClient;
	
	@Before
	public void createFhirClient() {
		fhirClient = FhirContext.forDstu3().newRestfulGenericClient(FHIR_TEST_ENDPOINT);
	}
	
	/*
	 * This test is based on the following example:
	 * http://hapifhir.io/doc_rest_client_examples.html
	 */
	@Test
	public void test() {
		
		Patient patient = createPatient();
		Observation observation = createObservation(patient);

		Bundle bundle = new Bundle();
		bundle.setType(BundleType.TRANSACTION);
		bundle.addEntry()
		   .setFullUrl(patient.getId())
		   .setResource(patient)
		   .getRequest()
		      .setUrl("Patient")
		      .setMethod(HTTPVerb.POST);
		bundle.addEntry()
		   .setResource(observation)
		   .getRequest()
		      .setUrl("Observation")
		      .setMethod(HTTPVerb.POST);
		
		logger.debug("Outgoing bundle: " + FhirUtilities.encodeResourceToXml(fhirClient.getFhirContext(), bundle));
		Bundle response = fhirClient.transaction().withBundle(bundle).execute();
		logger.debug("Response bundle: " + FhirUtilities.encodeResourceToXml(fhirClient.getFhirContext(), response));
		
		response.getEntry()
			.forEach(entry -> assertTrue(FhirUtilities.has2xxResponseStatus(entry)));
	}
	
	private Patient createPatient() {
		Patient patient = new Patient();
		patient.addIdentifier()
		   .setSystem("http://acme.org/mrns")
		   .setValue("12345");
		patient.addName()
		   .setFamily("Jameson")
		   .addGiven("J")
		   .addGiven("Jonah");
		patient.setGender(AdministrativeGender.MALE);
		patient.setId(IdDt.newRandomUuid());
		return patient;
	}
	
	private Observation createObservation(Patient patient) {
		Observation observation = new Observation();
		observation.setStatus(ObservationStatus.FINAL);
		observation
		   .getCode()
		      .addCoding()
		         .setSystem("http://loinc.org")
		         .setCode("789-8")
		         .setDisplay("Erythrocytes [#/volume] in Blood by Automated count");
		observation.setValue(
		   new Quantity()
		      .setValue(4.12)
		      .setUnit("10 trillion/L")
		      .setSystem("http://unitsofmeasure.org")
		      .setCode("10*12/L"));
		observation.setSubject(new Reference(patient));
		return observation;
	}
	
}
