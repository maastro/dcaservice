package nl.maastro.dcaservice.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import nl.maastro.dcaservice.DcaserviceApp;
import nl.maastro.dcaservice.domain.FhirTransactionLog;
import nl.maastro.dcaservice.domain.enumeration.TransactionStatus;
import nl.maastro.dcaservice.repository.FhirTransactionLogRepository;
import nl.maastro.dcaservice.web.rest.errors.ExceptionTranslator;
/**
 * Test class for the FhirTransactionLogResource REST controller.
 *
 * @see FhirTransactionLogResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DcaserviceApp.class)
public class FhirTransactionLogResourceIntTest {

    private static final Instant DEFAULT_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final TransactionStatus DEFAULT_STATUS = TransactionStatus.SUCCESS;
    private static final TransactionStatus UPDATED_STATUS = TransactionStatus.FAILED;

    private static final String DEFAULT_PLAN_UID = "AAAAAAAAAA";
    private static final String UPDATED_PLAN_UID = "BBBBBBBBBB";

    private static final String DEFAULT_OUTGOING_BUNDLE = "AAAAAAAAAA";
    private static final String UPDATED_OUTGOING_BUNDLE = "BBBBBBBBBB";

    private static final String DEFAULT_REPONSE_BUNDLE = "AAAAAAAAAA";
    private static final String UPDATED_REPONSE_BUNDLE = "BBBBBBBBBB";

    private static final String DEFAULT_ERROR_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_ERROR_MESSAGE = "BBBBBBBBBB";

    @Autowired
    private FhirTransactionLogRepository fhirTransactionLogRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restFhirTransactionLogMockMvc;

    private FhirTransactionLog fhirTransactionLog;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        FhirTransactionLogResource fhirTransactionLogResource = new FhirTransactionLogResource(fhirTransactionLogRepository);
        this.restFhirTransactionLogMockMvc = MockMvcBuilders.standaloneSetup(fhirTransactionLogResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FhirTransactionLog createEntity(EntityManager em) {
        FhirTransactionLog fhirTransactionLog = new FhirTransactionLog()
            .date(DEFAULT_DATE)
            .status(DEFAULT_STATUS)
            .planUid(DEFAULT_PLAN_UID)
            .outgoingBundle(DEFAULT_OUTGOING_BUNDLE)
            .reponseBundle(DEFAULT_REPONSE_BUNDLE)
            .errorMessage(DEFAULT_ERROR_MESSAGE);
        return fhirTransactionLog;
    }

    @Before
    public void initTest() {
        fhirTransactionLog = createEntity(em);
    }

    @Test
    @Transactional
    public void createFhirTransactionLog() throws Exception {
        int databaseSizeBeforeCreate = fhirTransactionLogRepository.findAll().size();

        // Create the FhirTransactionLog
        restFhirTransactionLogMockMvc.perform(post("/api/fhir-transaction-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fhirTransactionLog)))
            .andExpect(status().isCreated());

        // Validate the FhirTransactionLog in the database
        List<FhirTransactionLog> fhirTransactionLogList = fhirTransactionLogRepository.findAll();
        assertThat(fhirTransactionLogList).hasSize(databaseSizeBeforeCreate + 1);
        FhirTransactionLog testFhirTransactionLog = fhirTransactionLogList.get(fhirTransactionLogList.size() - 1);
        assertThat(testFhirTransactionLog.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testFhirTransactionLog.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testFhirTransactionLog.getPlanUid()).isEqualTo(DEFAULT_PLAN_UID);
        assertThat(testFhirTransactionLog.getOutgoingBundle()).isEqualTo(DEFAULT_OUTGOING_BUNDLE);
        assertThat(testFhirTransactionLog.getReponseBundle()).isEqualTo(DEFAULT_REPONSE_BUNDLE);
        assertThat(testFhirTransactionLog.getErrorMessage()).isEqualTo(DEFAULT_ERROR_MESSAGE);
    }

    @Test
    @Transactional
    public void createFhirTransactionLogWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = fhirTransactionLogRepository.findAll().size();

        // Create the FhirTransactionLog with an existing ID
        fhirTransactionLog.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFhirTransactionLogMockMvc.perform(post("/api/fhir-transaction-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fhirTransactionLog)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<FhirTransactionLog> fhirTransactionLogList = fhirTransactionLogRepository.findAll();
        assertThat(fhirTransactionLogList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllFhirTransactionLogs() throws Exception {
        // Initialize the database
        fhirTransactionLogRepository.saveAndFlush(fhirTransactionLog);

        // Get all the fhirTransactionLogList
        restFhirTransactionLogMockMvc.perform(get("/api/fhir-transaction-logs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fhirTransactionLog.getId().intValue())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].planUid").value(hasItem(DEFAULT_PLAN_UID.toString())))
            .andExpect(jsonPath("$.[*].outgoingBundle").value(hasItem(DEFAULT_OUTGOING_BUNDLE.toString())))
            .andExpect(jsonPath("$.[*].reponseBundle").value(hasItem(DEFAULT_REPONSE_BUNDLE.toString())))
            .andExpect(jsonPath("$.[*].errorMessage").value(hasItem(DEFAULT_ERROR_MESSAGE.toString())));
    }

    @Test
    @Transactional
    public void getFhirTransactionLog() throws Exception {
        // Initialize the database
        fhirTransactionLogRepository.saveAndFlush(fhirTransactionLog);

        // Get the fhirTransactionLog
        restFhirTransactionLogMockMvc.perform(get("/api/fhir-transaction-logs/{id}", fhirTransactionLog.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(fhirTransactionLog.getId().intValue()))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.planUid").value(DEFAULT_PLAN_UID.toString()))
            .andExpect(jsonPath("$.outgoingBundle").value(DEFAULT_OUTGOING_BUNDLE.toString()))
            .andExpect(jsonPath("$.reponseBundle").value(DEFAULT_REPONSE_BUNDLE.toString()))
            .andExpect(jsonPath("$.errorMessage").value(DEFAULT_ERROR_MESSAGE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingFhirTransactionLog() throws Exception {
        // Get the fhirTransactionLog
        restFhirTransactionLogMockMvc.perform(get("/api/fhir-transaction-logs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFhirTransactionLog() throws Exception {
        // Initialize the database
        fhirTransactionLogRepository.saveAndFlush(fhirTransactionLog);
        int databaseSizeBeforeUpdate = fhirTransactionLogRepository.findAll().size();

        // Update the fhirTransactionLog
        FhirTransactionLog updatedFhirTransactionLog = fhirTransactionLogRepository.findOne(fhirTransactionLog.getId());
        updatedFhirTransactionLog
            .date(UPDATED_DATE)
            .status(UPDATED_STATUS)
            .planUid(UPDATED_PLAN_UID)
            .outgoingBundle(UPDATED_OUTGOING_BUNDLE)
            .reponseBundle(UPDATED_REPONSE_BUNDLE)
            .errorMessage(UPDATED_ERROR_MESSAGE);

        restFhirTransactionLogMockMvc.perform(put("/api/fhir-transaction-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedFhirTransactionLog)))
            .andExpect(status().isOk());

        // Validate the FhirTransactionLog in the database
        List<FhirTransactionLog> fhirTransactionLogList = fhirTransactionLogRepository.findAll();
        assertThat(fhirTransactionLogList).hasSize(databaseSizeBeforeUpdate);
        FhirTransactionLog testFhirTransactionLog = fhirTransactionLogList.get(fhirTransactionLogList.size() - 1);
        assertThat(testFhirTransactionLog.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testFhirTransactionLog.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testFhirTransactionLog.getPlanUid()).isEqualTo(UPDATED_PLAN_UID);
        assertThat(testFhirTransactionLog.getOutgoingBundle()).isEqualTo(UPDATED_OUTGOING_BUNDLE);
        assertThat(testFhirTransactionLog.getReponseBundle()).isEqualTo(UPDATED_REPONSE_BUNDLE);
        assertThat(testFhirTransactionLog.getErrorMessage()).isEqualTo(UPDATED_ERROR_MESSAGE);
    }

    @Test
    @Transactional
    public void updateNonExistingFhirTransactionLog() throws Exception {
        int databaseSizeBeforeUpdate = fhirTransactionLogRepository.findAll().size();

        // Create the FhirTransactionLog

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restFhirTransactionLogMockMvc.perform(put("/api/fhir-transaction-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fhirTransactionLog)))
            .andExpect(status().isCreated());

        // Validate the FhirTransactionLog in the database
        List<FhirTransactionLog> fhirTransactionLogList = fhirTransactionLogRepository.findAll();
        assertThat(fhirTransactionLogList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteFhirTransactionLog() throws Exception {
        // Initialize the database
        fhirTransactionLogRepository.saveAndFlush(fhirTransactionLog);
        int databaseSizeBeforeDelete = fhirTransactionLogRepository.findAll().size();

        // Get the fhirTransactionLog
        restFhirTransactionLogMockMvc.perform(delete("/api/fhir-transaction-logs/{id}", fhirTransactionLog.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<FhirTransactionLog> fhirTransactionLogList = fhirTransactionLogRepository.findAll();
        assertThat(fhirTransactionLogList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FhirTransactionLog.class);
        FhirTransactionLog fhirTransactionLog1 = new FhirTransactionLog();
        fhirTransactionLog1.setId(1L);
        FhirTransactionLog fhirTransactionLog2 = new FhirTransactionLog();
        fhirTransactionLog2.setId(fhirTransactionLog1.getId());
        assertThat(fhirTransactionLog1).isEqualTo(fhirTransactionLog2);
        fhirTransactionLog2.setId(2L);
        assertThat(fhirTransactionLog1).isNotEqualTo(fhirTransactionLog2);
        fhirTransactionLog1.setId(null);
        assertThat(fhirTransactionLog1).isNotEqualTo(fhirTransactionLog2);
    }
}
