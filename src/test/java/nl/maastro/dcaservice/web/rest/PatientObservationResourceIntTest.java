package nl.maastro.dcaservice.web.rest;

import nl.maastro.dcaservice.DcaserviceApp;

import nl.maastro.dcaservice.domain.PatientObservation;
import nl.maastro.dcaservice.repository.PatientObservationRepository;
import nl.maastro.dcaservice.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PatientObservationResource REST controller.
 *
 * @see PatientObservationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DcaserviceApp.class)
public class PatientObservationResourceIntTest {

    private static final String DEFAULT_OBSERVATION_IDENTIFIER = "AAAAAAAAAA";
    private static final String UPDATED_OBSERVATION_IDENTIFIER = "BBBBBBBBBB";

    private static final String DEFAULT_OBSERVATION_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_OBSERVATION_VALUE = "BBBBBBBBBB";

    private static final Instant DEFAULT_OBSERVATION_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_OBSERVATION_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private PatientObservationRepository patientObservationRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPatientObservationMockMvc;

    private PatientObservation patientObservation;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PatientObservationResource patientObservationResource = new PatientObservationResource(patientObservationRepository);
        this.restPatientObservationMockMvc = MockMvcBuilders.standaloneSetup(patientObservationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PatientObservation createEntity(EntityManager em) {
        PatientObservation patientObservation = new PatientObservation()
            .observationIdentifier(DEFAULT_OBSERVATION_IDENTIFIER)
            .observationValue(DEFAULT_OBSERVATION_VALUE)
            .observationDate(DEFAULT_OBSERVATION_DATE);
        return patientObservation;
    }

    @Before
    public void initTest() {
        patientObservation = createEntity(em);
    }

    @Test
    @Transactional
    public void createPatientObservation() throws Exception {
        int databaseSizeBeforeCreate = patientObservationRepository.findAll().size();

        // Create the PatientObservation
        restPatientObservationMockMvc.perform(post("/api/patient-observations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(patientObservation)))
            .andExpect(status().isCreated());

        // Validate the PatientObservation in the database
        List<PatientObservation> patientObservationList = patientObservationRepository.findAll();
        assertThat(patientObservationList).hasSize(databaseSizeBeforeCreate + 1);
        PatientObservation testPatientObservation = patientObservationList.get(patientObservationList.size() - 1);
        assertThat(testPatientObservation.getObservationIdentifier()).isEqualTo(DEFAULT_OBSERVATION_IDENTIFIER);
        assertThat(testPatientObservation.getObservationValue()).isEqualTo(DEFAULT_OBSERVATION_VALUE);
        assertThat(testPatientObservation.getObservationDate()).isEqualTo(DEFAULT_OBSERVATION_DATE);
    }

    @Test
    @Transactional
    public void createPatientObservationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = patientObservationRepository.findAll().size();

        // Create the PatientObservation with an existing ID
        patientObservation.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPatientObservationMockMvc.perform(post("/api/patient-observations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(patientObservation)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<PatientObservation> patientObservationList = patientObservationRepository.findAll();
        assertThat(patientObservationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPatientObservations() throws Exception {
        // Initialize the database
        patientObservationRepository.saveAndFlush(patientObservation);

        // Get all the patientObservationList
        restPatientObservationMockMvc.perform(get("/api/patient-observations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(patientObservation.getId().intValue())))
            .andExpect(jsonPath("$.[*].observationIdentifier").value(hasItem(DEFAULT_OBSERVATION_IDENTIFIER.toString())))
            .andExpect(jsonPath("$.[*].observationValue").value(hasItem(DEFAULT_OBSERVATION_VALUE.toString())))
            .andExpect(jsonPath("$.[*].observationDate").value(hasItem(DEFAULT_OBSERVATION_DATE.toString())));
    }

    @Test
    @Transactional
    public void getPatientObservation() throws Exception {
        // Initialize the database
        patientObservationRepository.saveAndFlush(patientObservation);

        // Get the patientObservation
        restPatientObservationMockMvc.perform(get("/api/patient-observations/{id}", patientObservation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(patientObservation.getId().intValue()))
            .andExpect(jsonPath("$.observationIdentifier").value(DEFAULT_OBSERVATION_IDENTIFIER.toString()))
            .andExpect(jsonPath("$.observationValue").value(DEFAULT_OBSERVATION_VALUE.toString()))
            .andExpect(jsonPath("$.observationDate").value(DEFAULT_OBSERVATION_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPatientObservation() throws Exception {
        // Get the patientObservation
        restPatientObservationMockMvc.perform(get("/api/patient-observations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePatientObservation() throws Exception {
        // Initialize the database
        patientObservationRepository.saveAndFlush(patientObservation);
        int databaseSizeBeforeUpdate = patientObservationRepository.findAll().size();

        // Update the patientObservation
        PatientObservation updatedPatientObservation = patientObservationRepository.findOne(patientObservation.getId());
        updatedPatientObservation
            .observationIdentifier(UPDATED_OBSERVATION_IDENTIFIER)
            .observationValue(UPDATED_OBSERVATION_VALUE)
            .observationDate(UPDATED_OBSERVATION_DATE);

        restPatientObservationMockMvc.perform(put("/api/patient-observations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPatientObservation)))
            .andExpect(status().isOk());

        // Validate the PatientObservation in the database
        List<PatientObservation> patientObservationList = patientObservationRepository.findAll();
        assertThat(patientObservationList).hasSize(databaseSizeBeforeUpdate);
        PatientObservation testPatientObservation = patientObservationList.get(patientObservationList.size() - 1);
        assertThat(testPatientObservation.getObservationIdentifier()).isEqualTo(UPDATED_OBSERVATION_IDENTIFIER);
        assertThat(testPatientObservation.getObservationValue()).isEqualTo(UPDATED_OBSERVATION_VALUE);
        assertThat(testPatientObservation.getObservationDate()).isEqualTo(UPDATED_OBSERVATION_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingPatientObservation() throws Exception {
        int databaseSizeBeforeUpdate = patientObservationRepository.findAll().size();

        // Create the PatientObservation

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPatientObservationMockMvc.perform(put("/api/patient-observations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(patientObservation)))
            .andExpect(status().isCreated());

        // Validate the PatientObservation in the database
        List<PatientObservation> patientObservationList = patientObservationRepository.findAll();
        assertThat(patientObservationList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePatientObservation() throws Exception {
        // Initialize the database
        patientObservationRepository.saveAndFlush(patientObservation);
        int databaseSizeBeforeDelete = patientObservationRepository.findAll().size();

        // Get the patientObservation
        restPatientObservationMockMvc.perform(delete("/api/patient-observations/{id}", patientObservation.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PatientObservation> patientObservationList = patientObservationRepository.findAll();
        assertThat(patientObservationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PatientObservation.class);
        PatientObservation patientObservation1 = new PatientObservation();
        patientObservation1.setId(1L);
        PatientObservation patientObservation2 = new PatientObservation();
        patientObservation2.setId(patientObservation1.getId());
        assertThat(patientObservation1).isEqualTo(patientObservation2);
        patientObservation2.setId(2L);
        assertThat(patientObservation1).isNotEqualTo(patientObservation2);
        patientObservation1.setId(null);
        assertThat(patientObservation1).isNotEqualTo(patientObservation2);
    }
}
