package nl.maastro.dcaservice.web.rest;

import nl.maastro.dcaservice.DcaserviceApp;

import nl.maastro.dcaservice.domain.DicaRegistrationEntity;
import nl.maastro.dcaservice.repository.DicaRegistrationEntityRepository;
import nl.maastro.dcaservice.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import nl.maastro.dcaservice.domain.enumeration.DicaRegistration;
/**
 * Test class for the DicaRegistrationEntityResource REST controller.
 *
 * @see DicaRegistrationEntityResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DcaserviceApp.class)
public class DicaRegistrationEntityResourceIntTest {

    private static final DicaRegistration DEFAULT_DICA_REGISTRATION = DicaRegistration.DLCA;
    private static final DicaRegistration UPDATED_DICA_REGISTRATION = DicaRegistration.DLCA;

    @Autowired
    private DicaRegistrationEntityRepository dicaRegistrationEntityRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDicaRegistrationEntityMockMvc;

    private DicaRegistrationEntity dicaRegistrationEntity;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DicaRegistrationEntityResource dicaRegistrationEntityResource = new DicaRegistrationEntityResource(dicaRegistrationEntityRepository);
        this.restDicaRegistrationEntityMockMvc = MockMvcBuilders.standaloneSetup(dicaRegistrationEntityResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DicaRegistrationEntity createEntity(EntityManager em) {
        DicaRegistrationEntity dicaRegistrationEntity = new DicaRegistrationEntity()
            .dicaRegistration(DEFAULT_DICA_REGISTRATION);
        return dicaRegistrationEntity;
    }

    @Before
    public void initTest() {
        dicaRegistrationEntity = createEntity(em);
    }

    @Test
    @Transactional
    public void createDicaRegistrationEntity() throws Exception {
        int databaseSizeBeforeCreate = dicaRegistrationEntityRepository.findAll().size();

        // Create the DicaRegistrationEntity
        restDicaRegistrationEntityMockMvc.perform(post("/api/dica-registration-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dicaRegistrationEntity)))
            .andExpect(status().isCreated());

        // Validate the DicaRegistrationEntity in the database
        List<DicaRegistrationEntity> dicaRegistrationEntityList = dicaRegistrationEntityRepository.findAll();
        assertThat(dicaRegistrationEntityList).hasSize(databaseSizeBeforeCreate + 1);
        DicaRegistrationEntity testDicaRegistrationEntity = dicaRegistrationEntityList.get(dicaRegistrationEntityList.size() - 1);
        assertThat(testDicaRegistrationEntity.getDicaRegistration()).isEqualTo(DEFAULT_DICA_REGISTRATION);
    }

    @Test
    @Transactional
    public void createDicaRegistrationEntityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dicaRegistrationEntityRepository.findAll().size();

        // Create the DicaRegistrationEntity with an existing ID
        dicaRegistrationEntity.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDicaRegistrationEntityMockMvc.perform(post("/api/dica-registration-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dicaRegistrationEntity)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<DicaRegistrationEntity> dicaRegistrationEntityList = dicaRegistrationEntityRepository.findAll();
        assertThat(dicaRegistrationEntityList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllDicaRegistrationEntities() throws Exception {
        // Initialize the database
        dicaRegistrationEntityRepository.saveAndFlush(dicaRegistrationEntity);

        // Get all the dicaRegistrationEntityList
        restDicaRegistrationEntityMockMvc.perform(get("/api/dica-registration-entities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dicaRegistrationEntity.getId().intValue())))
            .andExpect(jsonPath("$.[*].dicaRegistration").value(hasItem(DEFAULT_DICA_REGISTRATION.toString())));
    }

    @Test
    @Transactional
    public void getDicaRegistrationEntity() throws Exception {
        // Initialize the database
        dicaRegistrationEntityRepository.saveAndFlush(dicaRegistrationEntity);

        // Get the dicaRegistrationEntity
        restDicaRegistrationEntityMockMvc.perform(get("/api/dica-registration-entities/{id}", dicaRegistrationEntity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dicaRegistrationEntity.getId().intValue()))
            .andExpect(jsonPath("$.dicaRegistration").value(DEFAULT_DICA_REGISTRATION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDicaRegistrationEntity() throws Exception {
        // Get the dicaRegistrationEntity
        restDicaRegistrationEntityMockMvc.perform(get("/api/dica-registration-entities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDicaRegistrationEntity() throws Exception {
        // Initialize the database
        dicaRegistrationEntityRepository.saveAndFlush(dicaRegistrationEntity);
        int databaseSizeBeforeUpdate = dicaRegistrationEntityRepository.findAll().size();

        // Update the dicaRegistrationEntity
        DicaRegistrationEntity updatedDicaRegistrationEntity = dicaRegistrationEntityRepository.findOne(dicaRegistrationEntity.getId());
        updatedDicaRegistrationEntity
            .dicaRegistration(UPDATED_DICA_REGISTRATION);

        restDicaRegistrationEntityMockMvc.perform(put("/api/dica-registration-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDicaRegistrationEntity)))
            .andExpect(status().isOk());

        // Validate the DicaRegistrationEntity in the database
        List<DicaRegistrationEntity> dicaRegistrationEntityList = dicaRegistrationEntityRepository.findAll();
        assertThat(dicaRegistrationEntityList).hasSize(databaseSizeBeforeUpdate);
        DicaRegistrationEntity testDicaRegistrationEntity = dicaRegistrationEntityList.get(dicaRegistrationEntityList.size() - 1);
        assertThat(testDicaRegistrationEntity.getDicaRegistration()).isEqualTo(UPDATED_DICA_REGISTRATION);
    }

    @Test
    @Transactional
    public void updateNonExistingDicaRegistrationEntity() throws Exception {
        int databaseSizeBeforeUpdate = dicaRegistrationEntityRepository.findAll().size();

        // Create the DicaRegistrationEntity

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDicaRegistrationEntityMockMvc.perform(put("/api/dica-registration-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dicaRegistrationEntity)))
            .andExpect(status().isCreated());

        // Validate the DicaRegistrationEntity in the database
        List<DicaRegistrationEntity> dicaRegistrationEntityList = dicaRegistrationEntityRepository.findAll();
        assertThat(dicaRegistrationEntityList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDicaRegistrationEntity() throws Exception {
        // Initialize the database
        dicaRegistrationEntityRepository.saveAndFlush(dicaRegistrationEntity);
        int databaseSizeBeforeDelete = dicaRegistrationEntityRepository.findAll().size();

        // Get the dicaRegistrationEntity
        restDicaRegistrationEntityMockMvc.perform(delete("/api/dica-registration-entities/{id}", dicaRegistrationEntity.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<DicaRegistrationEntity> dicaRegistrationEntityList = dicaRegistrationEntityRepository.findAll();
        assertThat(dicaRegistrationEntityList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DicaRegistrationEntity.class);
        DicaRegistrationEntity dicaRegistrationEntity1 = new DicaRegistrationEntity();
        dicaRegistrationEntity1.setId(1L);
        DicaRegistrationEntity dicaRegistrationEntity2 = new DicaRegistrationEntity();
        dicaRegistrationEntity2.setId(dicaRegistrationEntity1.getId());
        assertThat(dicaRegistrationEntity1).isEqualTo(dicaRegistrationEntity2);
        dicaRegistrationEntity2.setId(2L);
        assertThat(dicaRegistrationEntity1).isNotEqualTo(dicaRegistrationEntity2);
        dicaRegistrationEntity1.setId(null);
        assertThat(dicaRegistrationEntity1).isNotEqualTo(dicaRegistrationEntity2);
    }
}
