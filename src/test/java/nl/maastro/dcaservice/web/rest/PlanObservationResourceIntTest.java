package nl.maastro.dcaservice.web.rest;

import nl.maastro.dcaservice.DcaserviceApp;

import nl.maastro.dcaservice.domain.PlanObservation;
import nl.maastro.dcaservice.repository.PlanObservationRepository;
import nl.maastro.dcaservice.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PlanObservationResource REST controller.
 *
 * @see PlanObservationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DcaserviceApp.class)
public class PlanObservationResourceIntTest {

    private static final String DEFAULT_OBSERVATION_IDENTIFIER = "AAAAAAAAAA";
    private static final String UPDATED_OBSERVATION_IDENTIFIER = "BBBBBBBBBB";

    private static final String DEFAULT_OBSERVATION_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_OBSERVATION_VALUE = "BBBBBBBBBB";

    private static final Instant DEFAULT_OBSERVATION_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_OBSERVATION_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private PlanObservationRepository planObservationRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPlanObservationMockMvc;

    private PlanObservation planObservation;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PlanObservationResource planObservationResource = new PlanObservationResource(planObservationRepository);
        this.restPlanObservationMockMvc = MockMvcBuilders.standaloneSetup(planObservationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PlanObservation createEntity(EntityManager em) {
        PlanObservation planObservation = new PlanObservation()
            .observationIdentifier(DEFAULT_OBSERVATION_IDENTIFIER)
            .observationValue(DEFAULT_OBSERVATION_VALUE)
            .observationDate(DEFAULT_OBSERVATION_DATE);
        return planObservation;
    }

    @Before
    public void initTest() {
        planObservation = createEntity(em);
    }

    @Test
    @Transactional
    public void createPlanObservation() throws Exception {
        int databaseSizeBeforeCreate = planObservationRepository.findAll().size();

        // Create the PlanObservation
        restPlanObservationMockMvc.perform(post("/api/plan-observations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(planObservation)))
            .andExpect(status().isCreated());

        // Validate the PlanObservation in the database
        List<PlanObservation> planObservationList = planObservationRepository.findAll();
        assertThat(planObservationList).hasSize(databaseSizeBeforeCreate + 1);
        PlanObservation testPlanObservation = planObservationList.get(planObservationList.size() - 1);
        assertThat(testPlanObservation.getObservationIdentifier()).isEqualTo(DEFAULT_OBSERVATION_IDENTIFIER);
        assertThat(testPlanObservation.getObservationValue()).isEqualTo(DEFAULT_OBSERVATION_VALUE);
        assertThat(testPlanObservation.getObservationDate()).isEqualTo(DEFAULT_OBSERVATION_DATE);
    }

    @Test
    @Transactional
    public void createPlanObservationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = planObservationRepository.findAll().size();

        // Create the PlanObservation with an existing ID
        planObservation.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPlanObservationMockMvc.perform(post("/api/plan-observations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(planObservation)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<PlanObservation> planObservationList = planObservationRepository.findAll();
        assertThat(planObservationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPlanObservations() throws Exception {
        // Initialize the database
        planObservationRepository.saveAndFlush(planObservation);

        // Get all the planObservationList
        restPlanObservationMockMvc.perform(get("/api/plan-observations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(planObservation.getId().intValue())))
            .andExpect(jsonPath("$.[*].observationIdentifier").value(hasItem(DEFAULT_OBSERVATION_IDENTIFIER.toString())))
            .andExpect(jsonPath("$.[*].observationValue").value(hasItem(DEFAULT_OBSERVATION_VALUE.toString())))
            .andExpect(jsonPath("$.[*].observationDate").value(hasItem(DEFAULT_OBSERVATION_DATE.toString())));
    }

    @Test
    @Transactional
    public void getPlanObservation() throws Exception {
        // Initialize the database
        planObservationRepository.saveAndFlush(planObservation);

        // Get the planObservation
        restPlanObservationMockMvc.perform(get("/api/plan-observations/{id}", planObservation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(planObservation.getId().intValue()))
            .andExpect(jsonPath("$.observationIdentifier").value(DEFAULT_OBSERVATION_IDENTIFIER.toString()))
            .andExpect(jsonPath("$.observationValue").value(DEFAULT_OBSERVATION_VALUE.toString()))
            .andExpect(jsonPath("$.observationDate").value(DEFAULT_OBSERVATION_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPlanObservation() throws Exception {
        // Get the planObservation
        restPlanObservationMockMvc.perform(get("/api/plan-observations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePlanObservation() throws Exception {
        // Initialize the database
        planObservationRepository.saveAndFlush(planObservation);
        int databaseSizeBeforeUpdate = planObservationRepository.findAll().size();

        // Update the planObservation
        PlanObservation updatedPlanObservation = planObservationRepository.findOne(planObservation.getId());
        updatedPlanObservation
            .observationIdentifier(UPDATED_OBSERVATION_IDENTIFIER)
            .observationValue(UPDATED_OBSERVATION_VALUE)
            .observationDate(UPDATED_OBSERVATION_DATE);

        restPlanObservationMockMvc.perform(put("/api/plan-observations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPlanObservation)))
            .andExpect(status().isOk());

        // Validate the PlanObservation in the database
        List<PlanObservation> planObservationList = planObservationRepository.findAll();
        assertThat(planObservationList).hasSize(databaseSizeBeforeUpdate);
        PlanObservation testPlanObservation = planObservationList.get(planObservationList.size() - 1);
        assertThat(testPlanObservation.getObservationIdentifier()).isEqualTo(UPDATED_OBSERVATION_IDENTIFIER);
        assertThat(testPlanObservation.getObservationValue()).isEqualTo(UPDATED_OBSERVATION_VALUE);
        assertThat(testPlanObservation.getObservationDate()).isEqualTo(UPDATED_OBSERVATION_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingPlanObservation() throws Exception {
        int databaseSizeBeforeUpdate = planObservationRepository.findAll().size();

        // Create the PlanObservation

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPlanObservationMockMvc.perform(put("/api/plan-observations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(planObservation)))
            .andExpect(status().isCreated());

        // Validate the PlanObservation in the database
        List<PlanObservation> planObservationList = planObservationRepository.findAll();
        assertThat(planObservationList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePlanObservation() throws Exception {
        // Initialize the database
        planObservationRepository.saveAndFlush(planObservation);
        int databaseSizeBeforeDelete = planObservationRepository.findAll().size();

        // Get the planObservation
        restPlanObservationMockMvc.perform(delete("/api/plan-observations/{id}", planObservation.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PlanObservation> planObservationList = planObservationRepository.findAll();
        assertThat(planObservationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PlanObservation.class);
        PlanObservation planObservation1 = new PlanObservation();
        planObservation1.setId(1L);
        PlanObservation planObservation2 = new PlanObservation();
        planObservation2.setId(planObservation1.getId());
        assertThat(planObservation1).isEqualTo(planObservation2);
        planObservation2.setId(2L);
        assertThat(planObservation1).isNotEqualTo(planObservation2);
        planObservation1.setId(null);
        assertThat(planObservation1).isNotEqualTo(planObservation2);
    }
}
