package nl.maastro.dcaservice.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import nl.maastro.dcaservice.DcaserviceApp;
import nl.maastro.dcaservice.domain.RnvPlan;
import nl.maastro.dcaservice.repository.RnvPlanRepository;
import nl.maastro.dcaservice.service.DlcaRegistrationService;
import nl.maastro.dcaservice.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the RnvPlanResource REST controller.
 *
 * @see RnvPlanResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DcaserviceApp.class)
public class RnvPlanResourceIntTest {

    private static final String DEFAULT_PLAN_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_PLAN_LABEL = "BBBBBBBBBB";

    private static final String DEFAULT_PLAN_SOP_INSTANCE_UID = "AAAAAAAAAA";
    private static final String UPDATED_PLAN_SOP_INSTANCE_UID = "BBBBBBBBBB";

    private static final String DEFAULT_STUDY_INSTANCE_UID = "AAAAAAAAAA";
    private static final String UPDATED_STUDY_INSTANCE_UID = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_CREATED = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_CREATED = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private RnvPlanRepository rnvPlanRepository;

    @Autowired
    private DlcaRegistrationService dlcaRegistrationService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restRnvPlanMockMvc;

    private RnvPlan rnvPlan;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        RnvPlanResource rnvPlanResource = new RnvPlanResource(rnvPlanRepository, 
                dlcaRegistrationService);
        this.restRnvPlanMockMvc = MockMvcBuilders.standaloneSetup(rnvPlanResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RnvPlan createEntity(EntityManager em) {
        RnvPlan rnvPlan = new RnvPlan()
            .planLabel(DEFAULT_PLAN_LABEL)
            .planSopInstanceUid(DEFAULT_PLAN_SOP_INSTANCE_UID)
            .studyInstanceUid(DEFAULT_STUDY_INSTANCE_UID)
            .dateCreated(DEFAULT_DATE_CREATED);
        return rnvPlan;
    }

    @Before
    public void initTest() {
        rnvPlan = createEntity(em);
    }

    @Test
    @Transactional
    public void createRnvPlan() throws Exception {
        int databaseSizeBeforeCreate = rnvPlanRepository.findAll().size();

        // Create the RnvPlan
        restRnvPlanMockMvc.perform(post("/api/rnv-plans")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rnvPlan)))
            .andExpect(status().isCreated());

        // Validate the RnvPlan in the database
        List<RnvPlan> rnvPlanList = rnvPlanRepository.findAll();
        assertThat(rnvPlanList).hasSize(databaseSizeBeforeCreate + 1);
        RnvPlan testRnvPlan = rnvPlanList.get(rnvPlanList.size() - 1);
        assertThat(testRnvPlan.getPlanLabel()).isEqualTo(DEFAULT_PLAN_LABEL);
        assertThat(testRnvPlan.getPlanSopInstanceUid()).isEqualTo(DEFAULT_PLAN_SOP_INSTANCE_UID);
        assertThat(testRnvPlan.getStudyInstanceUid()).isEqualTo(DEFAULT_STUDY_INSTANCE_UID);
        assertThat(testRnvPlan.getDateCreated()).isEqualTo(DEFAULT_DATE_CREATED);
    }

    @Test
    @Transactional
    public void createRnvPlanWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = rnvPlanRepository.findAll().size();

        // Create the RnvPlan with an existing ID
        rnvPlan.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRnvPlanMockMvc.perform(post("/api/rnv-plans")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rnvPlan)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<RnvPlan> rnvPlanList = rnvPlanRepository.findAll();
        assertThat(rnvPlanList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllRnvPlans() throws Exception {
        // Initialize the database
        rnvPlanRepository.saveAndFlush(rnvPlan);

        // Get all the rnvPlanList
        restRnvPlanMockMvc.perform(get("/api/rnv-plans?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(rnvPlan.getId().intValue())))
            .andExpect(jsonPath("$.[*].planLabel").value(hasItem(DEFAULT_PLAN_LABEL.toString())))
            .andExpect(jsonPath("$.[*].planSopInstanceUid").value(hasItem(DEFAULT_PLAN_SOP_INSTANCE_UID.toString())))
            .andExpect(jsonPath("$.[*].studyInstanceUid").value(hasItem(DEFAULT_STUDY_INSTANCE_UID.toString())))
            .andExpect(jsonPath("$.[*].dateCreated").value(hasItem(DEFAULT_DATE_CREATED.toString())));
    }

    @Test
    @Transactional
    public void getRnvPlan() throws Exception {
        // Initialize the database
        rnvPlanRepository.saveAndFlush(rnvPlan);

        // Get the rnvPlan
        restRnvPlanMockMvc.perform(get("/api/rnv-plans/{id}", rnvPlan.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(rnvPlan.getId().intValue()))
            .andExpect(jsonPath("$.planLabel").value(DEFAULT_PLAN_LABEL.toString()))
            .andExpect(jsonPath("$.planSopInstanceUid").value(DEFAULT_PLAN_SOP_INSTANCE_UID.toString()))
            .andExpect(jsonPath("$.studyInstanceUid").value(DEFAULT_STUDY_INSTANCE_UID.toString()))
            .andExpect(jsonPath("$.dateCreated").value(DEFAULT_DATE_CREATED.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingRnvPlan() throws Exception {
        // Get the rnvPlan
        restRnvPlanMockMvc.perform(get("/api/rnv-plans/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRnvPlan() throws Exception {
        // Initialize the database
        rnvPlanRepository.saveAndFlush(rnvPlan);
        int databaseSizeBeforeUpdate = rnvPlanRepository.findAll().size();

        // Update the rnvPlan
        RnvPlan updatedRnvPlan = rnvPlanRepository.findOne(rnvPlan.getId());
        updatedRnvPlan
            .planLabel(UPDATED_PLAN_LABEL)
            .planSopInstanceUid(UPDATED_PLAN_SOP_INSTANCE_UID)
            .studyInstanceUid(UPDATED_STUDY_INSTANCE_UID)
            .dateCreated(UPDATED_DATE_CREATED);

        restRnvPlanMockMvc.perform(put("/api/rnv-plans")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRnvPlan)))
            .andExpect(status().isOk());

        // Validate the RnvPlan in the database
        List<RnvPlan> rnvPlanList = rnvPlanRepository.findAll();
        assertThat(rnvPlanList).hasSize(databaseSizeBeforeUpdate);
        RnvPlan testRnvPlan = rnvPlanList.get(rnvPlanList.size() - 1);
        assertThat(testRnvPlan.getPlanLabel()).isEqualTo(UPDATED_PLAN_LABEL);
        assertThat(testRnvPlan.getPlanSopInstanceUid()).isEqualTo(UPDATED_PLAN_SOP_INSTANCE_UID);
        assertThat(testRnvPlan.getStudyInstanceUid()).isEqualTo(UPDATED_STUDY_INSTANCE_UID);
        assertThat(testRnvPlan.getDateCreated()).isEqualTo(UPDATED_DATE_CREATED);
    }

    @Test
    @Transactional
    public void updateNonExistingRnvPlan() throws Exception {
        int databaseSizeBeforeUpdate = rnvPlanRepository.findAll().size();

        // Create the RnvPlan

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restRnvPlanMockMvc.perform(put("/api/rnv-plans")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rnvPlan)))
            .andExpect(status().isCreated());

        // Validate the RnvPlan in the database
        List<RnvPlan> rnvPlanList = rnvPlanRepository.findAll();
        assertThat(rnvPlanList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteRnvPlan() throws Exception {
        // Initialize the database
        rnvPlanRepository.saveAndFlush(rnvPlan);
        int databaseSizeBeforeDelete = rnvPlanRepository.findAll().size();

        // Get the rnvPlan
        restRnvPlanMockMvc.perform(delete("/api/rnv-plans/{id}", rnvPlan.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<RnvPlan> rnvPlanList = rnvPlanRepository.findAll();
        assertThat(rnvPlanList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RnvPlan.class);
        RnvPlan rnvPlan1 = new RnvPlan();
        rnvPlan1.setId(1L);
        RnvPlan rnvPlan2 = new RnvPlan();
        rnvPlan2.setId(rnvPlan1.getId());
        assertThat(rnvPlan1).isEqualTo(rnvPlan2);
        rnvPlan2.setId(2L);
        assertThat(rnvPlan1).isNotEqualTo(rnvPlan2);
        rnvPlan1.setId(null);
        assertThat(rnvPlan1).isNotEqualTo(rnvPlan2);
    }
}
